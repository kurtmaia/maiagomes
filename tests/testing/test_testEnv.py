import json
import os
from datetime import datetime, timezone
from glob import glob

import numpy as np
import pandas as pd
from alpaca.data.historical import CryptoHistoricalDataClient, StockHistoricalDataClient
from alpaca.trading.client import TradingClient

from maiagomes.data import Pull_bars_from_alpaca
from maiagomes.models.allocate import (
    get_arma_portfolio,
    get_mmar_portfolio,
    get_random_portfolio,
    get_simple_portfolio,
)
from maiagomes.testing import Data_handler, testEnv

time_series_df_path = "../investing_poc/data/mining/merged/all_quotes.csv"

symbols = json.load(open("target_symbols.json", "rb"))

stock_client = StockHistoricalDataClient(
    os.getenv("APCA_API_KEY_ID"), os.getenv("APCA_API_SECRET_KEY")
)
crypto_client = CryptoHistoricalDataClient()
trading_client = TradingClient(
    os.getenv("APCA_API_KEY_ID"), os.getenv("APCA_API_SECRET_KEY"), paper=True
)
latest_values = Pull_bars_from_alpaca(
    stock_client, crypto_client, start=datetime(2024, 10, 1)
).pull(symbols=symbols["crypto"] + symbols["stock"])

latest_values["Date"] = latest_values["timestamp"].map(lambda a: a.replace(tzinfo=None))
latest_values.columns = [v.replace("/USD", "") for v in latest_values]
df = latest_values.loc[
    latest_values["timestamp"] >= datetime(2024, 10, 1).astimezone(),
].drop(columns=["timestamp"])


mmar_results = {}
for __file__ in glob("../data/old_model_results/*"):
    date = __file__.split("/")[-1].split("_")[-1].replace(".json", "")
    _ = json.load(open(__file__, "rb"))
    mmar_results[date] = json.load(open(__file__, "rb"))


def test_arma():
    # Arange
    data = Data_handler(df=df.dropna(), date_var="Date", add_cash=True)
    test = testEnv(data, interval_period="D")

    def get_mmar_portfolio_wrapper(
        *args, mmar_results=mmar_results, symbolList=test.data.symbolList
    ):
        return get_mmar_portfolio(
            *args, mmar_results=mmar_results, symbolList=symbolList
        )

    def get_arma_portfolio_wrapper(
        *args, mmar_results=mmar_results, symbolList=test.data.symbolList
    ):
        return get_arma_portfolio(
            *args,
            p=-1,
            d=1,
            q=1,
        )

    strategies = {
        "market": get_simple_portfolio,
        "random": get_random_portfolio,
        # "mmar": get_mmar_portfolio_wrapper,
        "arma": get_arma_portfolio_wrapper,
    }

    # Act
    rois = test.simulate(
        strategies=strategies, reinvest_amount=0, buy_and_hold=False, showProgress=True
    )

    # Assert
    assert pd.DataFrame(rois[0]).idxmax(axis=1).tolist()[-1] == "arma"
