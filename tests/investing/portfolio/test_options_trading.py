import pytest
from datetime import datetime
from maiagomes.investing.portfolio.options_trading import (
    get_expiration_date_from_symbol,
)


@pytest.mark.parametrize(
    "symbol, expected_date",
    [
        ("FCX250103P00039000", datetime.strptime("250103", "%y%m%d").date()),
        ("AAPL250103C00039000", datetime.strptime("250103", "%y%m%d").date()),
        ("COP250103C00045000", datetime.strptime("250103", "%y%m%d").date()),
        ("3M250103P00045000", datetime.strptime("250103", "%y%m%d").date()),
    ],
)
def test_get_expiration_date_from_symbol(symbol, expected_date):
    assert get_expiration_date_from_symbol(symbol) == expected_date
