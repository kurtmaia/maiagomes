import sys

import pytest

from maiagomes.cli.investor import options_trading, rebalance_portfolio_using_arma


@pytest.mark.parametrize(
    "config",
    [
        (
            [
                "maiagomes-rebalance-arma",
                "--mmar-results-file",
                "results/mmar/latest_results.json",
                "--target-symbols",
                "target_symbols.json",
                "--paper",
                "--percent-equity-to-trade",
                "0.5",
            ]
        ),
        (
            [
                "maiagomes-rebalance-arma",
                "--mmar-results-file",
                "results/mmar/latest_results.json",
                "--target-symbols",
                "target_symbols.json",
                "--paper",
                "--paper-profile",
                "mid",
                "--percent-equity-to-trade",
                "0.5",
            ]
        ),
        (
            [
                "maiagomes-rebalance-arma",
                "--mmar-results-file",
                "results/mmar/latest_results.json",
                "--target-symbols",
                "target_symbols.json",
                "--paper",
                "--paper-profile",
                "high",
                "--percent-equity-to-trade",
                "0.5",
            ]
        ),
        (
            [
                "maiagomes-rebalance-arma",
                "--mmar-results-file",
                "results/mmar/latest_results.json",
                "--target-symbols",
                "target_symbols.json",
            ]
        ),
    ],
    ids=["paper", "mid", "high", "live"],
)
def test_rebalance_portfolio_using_arma(config, monkeypatch):
    monkeypatch.setattr(sys, "argv", config)  # Set sys.argv to fake_args

    result = rebalance_portfolio_using_arma()
    assert result == "completed"


@pytest.mark.parametrize(
    "config",
    [
        (
            [
                "maiagomes-option-trader",
                "-v",
                "--options-assessment-file",
                "results/assessment/_full_.csv",
                "--target-symbols",
                "target_symbols.json",
                "--paper",
                "--percent-equity-to-trade",
                "0.5",
            ]
        ),
        (
            [
                "maiagomes-option-trader",
                "-v",
                "--options-assessment-file",
                "results/assessment/_full_.csv",
                "--target-symbols",
                "target_symbols.json",
                "--paper",
                "--paper-profile",
                "mid",
                "--percent-equity-to-trade",
                "0.5",
            ]
        ),
        (
            [
                "maiagomes-option-trader",
                "-v",
                "--options-assessment-file",
                "results/assessment/_full_.csv",
                "--target-symbols",
                "target_symbols.json",
                "--paper",
                "--paper-profile",
                "high",
                "--percent-equity-to-trade",
                "0.5",
            ]
        ),
        (
            [
                "maiagomes-option-trader",
                "--options-assessment-file",
                "results/assessment/_full_.csv",
                "--target-symbols",
                "target_symbols.json",
                "--percent-equity-to-trade",
                "0.5",
            ]
        ),
    ],
    ids=["paper", "mid", "high", "live"],
)
def test_option_trading(config, monkeypatch):
    monkeypatch.setattr(sys, "argv", config)  # Set sys.argv to fake_args

    result = options_trading()
