import sys

import pytest

from maiagomes.cli.simulator import option_contract_assessment_runner


@pytest.mark.parametrize(
    "config",
    [
        (
            [
                "maiagomes-option-assessment",
                "-v",
                "--mmar-results-file",
                "tests/cli/results/mmar/latest_results.json",
                "--paths-folder",
                "tests/cli/results/simulated_paths/",
                "--assessment-folder",
                "tests/cli/results/assessment/",
                "--persistent",
                "--rank-max",
                "5",
                "--parallel",
                "--n-paths",
                "1000",
                "--weeks",
                "2",
            ]
        )
    ],
)
def test_option_contract_assessment_runner(config, monkeypatch):
    monkeypatch.setattr(sys, "argv", config)  # Set sys.argv to fake_args

    result = option_contract_assessment_runner()
