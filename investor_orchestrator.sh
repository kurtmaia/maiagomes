#!/bin/bash


# Check if the required arguments are provided
if [ "$#" -ne 6 ]; then
    echo "Usage: $0 target_symbols_file mmar_results_file paths_folder assessment_folder options_assessment_file n_paths"
    exit 1
fi

# Assign command-line arguments to variables
target_symbols_file=$1
mmar_results_file=$2
paths_folder=$3
assessment_folder=$4
options_assessment_file=$5
n_paths=$6

# ####################
# ##### INVESTING ####
# ####################
maiagomes-rebalance-arma --mmar-results-file "$mmar_results_file" --target-symbols "$target_symbols_file" --paper -p -1 --percent-equity-to-trade 0.5
maiagomes-rebalance-arma --mmar-results-file "$mmar_results_file" --target-symbols "$target_symbols_file" --paper --paper-profile mid
maiagomes-rebalance-arma --mmar-results-file "$mmar_results_file" --target-symbols "$target_symbols_file" --paper --paper-profile high
maiagomes-rebalance-arma --mmar-results-file "$mmar_results_file" --target-symbols "$target_symbols_file" 

# ##############################
# ##### OPTIONS ASSESSMENT #####
# ##############################
maiagomes-option-assessment -v --mmar-results-file "$mmar_results_file" --paths-folder "$paths_folder" --assessment-folder "$assessment_folder" --persistent --rank-max 5  --n-paths "$n_paths" --weeks 2 --parallel

# ###########################
# ##### OPTIONS TRADING #####
# ###########################
maiagomes-option-trader --options-assessment-file "$options_assessment_file" --target-symbols "$target_symbols_file" --paper --percent-equity-to-trade 1.0 --min-cost .2 --max-cost 5.0 # --univariate-allocation-variable "kelly_c_50"  --rebalance-options
maiagomes-option-trader --options-assessment-file "$options_assessment_file" --target-symbols "$target_symbols_file" --paper --paper-profile mid --percent-equity-to-trade 0.1 --min-cost .3 --max-cost 20.0 # --univariate-allocation-variable "kelly_c_50"  --rebalance-options
maiagomes-option-trader --options-assessment-file "$options_assessment_file" --target-symbols "$target_symbols_file" --paper --paper-profile high --percent-equity-to-trade 0.05 --min-cost .3 --max-cost 20.0 # --univariate-allocation-variable "kelly_c_50"  --rebalance-options
maiagomes-option-trader --options-assessment-file "$options_assessment_file" --target-symbols "$target_symbols_file" --percent-equity-to-trade 0.5 --min-cost .3 --max-cost 20.0  # --univariate-allocation-variable "kelly_c_50"  --rebalance-options

# ### END OF FILE
