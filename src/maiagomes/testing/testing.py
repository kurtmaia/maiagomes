import logging
import pickle as pk
from datetime import datetime

import numpy as np
import pandas as pd
from scipy.stats import binom, dirichlet, multinomial, norm, uniform
from tqdm import tqdm

logger = logging.getLogger(__name__)


def naomit(x):
    return x[~np.isnan(x)]


class Data_handler(object):
    def __init__(
        self, df=None, df_path=None, date_var="Data", add_cash=False, filter_out=None
    ):
        assert (
            df is not None or df_path is not None
        ), "Either df or df_path needs to be defined."
        if df_path is not None:
            df = pd.read_csv(df_path)
        self.date_var = date_var
        df[date_var] = pd.to_datetime(df[date_var])
        df.sort_values(by=date_var, inplace=True)
        df.ffill(inplace=True)
        self.symbolList = list(set(df.columns) - set([date_var]))
        if add_cash:
            df["cash"] = 1
            self.symbolList += ["cash"]
        if filter_out is not None:
            df = df.query(filter_out)
        self.date_var = date_var
        self.df = df[[date_var] + self.symbolList]
        self.df.reset_index(inplace=True)
        self.df.drop("index", axis=1, inplace=True)
        self.df = self.df.groupby(date_var).mean()
        self.df.reset_index(inplace=True)
        self.matrix_form = self.df[self.symbolList].values

    def dates(self):
        return self.df[self.date_var]

    def get_last_events_by(self, group_mode="M"):
        last_event_indices = self.df.groupby(
            self.df[self.date_var].dt.to_period(group_mode)
        )[self.date_var].idxmax()
        last_events = self.df.loc[last_event_indices]
        return last_events

    def subset(self, df=None, first=None, last=None):
        if df is None:
            df = self.df
        if first is None:
            first = df[self.date_var].values[0]
        if last is None:
            last = df[self.date_var].values[-1]
        return df.set_index(self.date_var).loc[first:last, self.symbolList]


def select_invest_time(arr, type="random"):
    if type == "random":
        return np.random.choice(arr)
    elif type == "median":
        return arr[int(arr.shape[0] / 2)]
    elif type == "last":
        return arr[-1]
    elif type == "first":
        return arr[0]


def get_random_portfolio(historyPrices):
    vals = np.isnan(historyPrices[-1,]) == False
    N = sum(vals)
    p = vals / N
    return (multinomial.rvs(n=N, p=p) / N).reshape([1, -1])


class estimate_W(object):
    def __init__(self, strategies):
        self.strategies = strategies

    def get(self, strategy, historyPrices, currDate, **kwargs):
        return self.strategies[strategy](historyPrices, currDate, **kwargs)


def getW(strategy, historyPrices):
    return get_random_portfolio(historyPrices)


def getPortfolio(
    W, current_portfolio, oldPrices, currPrices, reinvest_amount=0, buy_and_hold=False
):
    props = np.nan_to_num(currPrices / oldPrices)
    updateCurrent_portfolio = current_portfolio * props
    ans = updateCurrent_portfolio * buy_and_hold + W * (
        reinvest_amount + updateCurrent_portfolio.sum() * (1 - buy_and_hold)
    )
    return ans


class testEnv(object):
    def __init__(self, data, **kwargs):
        self.data = data

        self.prepdates(**kwargs)

    def prepdates(self, interval_period="M", within_period="median"):
        ### Define the dates for testing based on setting
        dates = self.data.dates().values
        datearr = pd.date_range(dates[0], dates[-1], freq=interval_period).values
        for i in range(1, datearr.shape[0]):
            tmp = select_invest_time(
                pd.date_range(datearr[i - 1], datearr[i], freq="min").values,
                type=within_period,
            )
            datearr[i] = dates[np.abs((tmp - dates)).argmin()]
        self.datearr = datearr[1:]
        self.prices = self.data.df.set_index(self.data.date_var).loc[datearr,].values

    def simulate(
        self,
        strategies={"random": "get_random_portfolio", "mm": "get_random_portfolio"},
        init_invest=1,
        init_portfolio=None,
        reinvest_amount=0,
        buy_and_hold=False,
        showProgress=False,
    ):
        data = self.data
        datearr = self.datearr
        prices = self.prices
        nSym = len(self.data.symbolList)
        ### set initial portfolios randomly
        if init_portfolio is None:
            # init_portfolio = multinomial.rvs(
            #     n=1, p=np.array([1.0 / nSym] * nSym)
            # ).reshape([1, -1])
            init_portfolio = np.array([1.0 / nSym] * nSym).reshape([1, -1])
        portfolios = {s: init_invest * init_portfolio for s in strategies}
        returns = {s: {} for s in strategies.keys()}
        total_investiment = init_invest
        w_estimator = estimate_W(strategies=strategies)
        for idx in tqdm(range(1, datearr.shape[0]), disable=not showProgress):
            currDate = datearr[idx]
            mat = data.subset(data.get_last_events_by("D"), last=currDate).values
            historyPrices = mat[:-1,]
            currPrices = mat[-1,]
            total_investiment += reinvest_amount
            for strategy in strategies.keys():
                W = w_estimator.get(strategy, historyPrices, currDate)
                port = getPortfolio(
                    W,
                    portfolios[strategy][idx - 1,],
                    prices[idx - 1,],
                    currPrices,
                    reinvest_amount=reinvest_amount,
                    buy_and_hold=buy_and_hold,
                )
                portfolios[strategy] = np.vstack([portfolios[strategy], port])
                returns[strategy][currDate] = port.sum()
        return returns, portfolios, total_investiment

    def sample(self, nSamples=1000, **kwargs):
        results = {}
        for sample in tqdm(range(nSamples)):
            rr, _, t = self.simulate(**kwargs)
            results[sample] = {"returns": rr, "portfolios": _, "total_investiment": t}
        return results
