import datetime
import json
import logging
import os
import sys
from typing import Callable, Dict, Iterator, List, Optional, Union

import numpy as np
import pandas as pd
from alpaca.data.timeframe import TimeFrame
from dotenv import load_dotenv
from tqdm import tqdm

from maiagomes.data.alpaca.pull import Backfill_minute_bars

from .utils import parse_arguments

load_dotenv()


def update_symbol_quotes(
    symbol: str,
    symbol_type: str = "stock",
    start: datetime = datetime.datetime(2021, 2, 5),
    end: datetime = None,
    timeframe: Optional[TimeFrame] = TimeFrame.Minute,
    cache_folder: str = None,
):
    Backfill_minute_bars(
        symbol=symbol,
        symbol_type=symbol_type,
        start=start,
        end=end,
        timeframe=timeframe,
        cache_folder=cache_folder,
    ).update()


def update_all_symbols_with_a_config_file():
    args = parse_arguments(sys.argv[1:])
    logging.basicConfig(level=args.loglevel)
    target_symbols = json.load(open(args.target_symbols, "r"))
    for symbol_type, symbols in target_symbols.items():
        for symbol in tqdm(symbols):
            update_symbol_quotes(
                symbol=symbol, symbol_type=symbol_type, cache_folder=args.cache_folder
            )
