import argparse
import logging

from alpaca.trading.client import TradingClient


def parse_arguments(argv):
    parser = argparse.ArgumentParser(description="Arguments available")
    #### GENERAL ####
    parser.add_argument(
        "--mmar-results-file",
        help="json file with mmar results",
        type=str,
    )
    parser.add_argument(
        "--options-assessment-file",
        help="dataframe file with options assessment",
        type=str,
    )
    parser.add_argument(
        "-v",
        "--verbose",
        help="Be verbose",
        action="store_const",
        dest="loglevel",
        const=logging.INFO,
    )
    parser.add_argument(
        "-vv",
        "--debug",
        help="Be verbose",
        action="store_const",
        dest="loglevel",
        const=logging.DEBUG,
    )
    #### INVESTOR ####
    parser.add_argument(
        "--time-series-df-path",
        help="time series df path",
        type=str,
    )
    parser.add_argument(
        "--api_key_id",
        type=str,
        help="Alpaca API Key ID",
    )
    parser.add_argument(
        "--api_secret_key",
        type=str,
        help="Alpaca API Secret Key",
    )
    parser.add_argument(
        "--paper", action="store_true", help="Use paper trading (default: True)"
    )
    parser.add_argument(
        "--paper-profile",
        type=str,
        help="Paper profile to be used in trading. Options (paper, mid, high).",
        default="paper",
    )
    parser.add_argument(
        "--total-tradable-money", type=float, help="Total tradable money", default=None
    )
    parser.add_argument("--percent-equity-to-trade", type=float, default=None)
    parser.add_argument("--univariate-allocation-variable", type=str, default="kelly_c")
    parser.add_argument("--rebalance-options", action="store_true")
    parser.add_argument("-p", type=float, help="arima p parameter", default=2.0)
    parser.add_argument("-d", type=float, help="arima d parameter", default=1.0)
    parser.add_argument("-q", type=float, help="arima q parameter", default=1.0)
    parser.add_argument(
        "--min-cost",
        type=float,
        help="Minimum cost of each option contract",
        default=0.3,
    )
    parser.add_argument(
        "--max-cost",
        type=float,
        help="Maximum cost of each option contract",
        default=0.5,
    )

    #### SIMULATOR ####
    parser.add_argument(
        "--paths-folder",
        type=str,
        help="Folder where the simulated paths will be cached",
    )
    parser.add_argument(
        "--assessment-folder",
        type=str,
        help="Folder where the assessed contract will be stored",
    )
    parser.add_argument(
        "--weeks",
        type=int,
        help="How many Fridays from the next one",
        default=1,
    )
    parser.add_argument(
        "--n-paths",
        type=int,
        help="Number of paths to simulate. Default 1000.",
        default=1000,
    )
    parser.add_argument(
        "--min-thr",
        type=int,
        help="Minimum number of minutes in a targeted return. Default 390 (a trading day).",
        default=390,
    )
    parser.add_argument("--rank-max", type=int, help="Max rank")

    parser.add_argument(
        "--parallel", action="store_true", help="Run assessment in parallel."
    )
    parser.add_argument(
        "--persistent", action="store_true", help="Keep trying to assess other symbols."
    )

    #### DATA PULLER AND ESTIMATOR ####

    parser.add_argument(
        "--target-symbols",
        type=str,
        help="File with symbol names splitted by symbol type (stock or crypto).",
    )

    parser.add_argument(
        "--cache-folder",
        type=str,
        help="Folder where the data pulled will be stored",
    )

    return parser.parse_args(argv)
