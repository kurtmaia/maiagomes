import json
import logging
import multiprocessing
import os
import sys
import warnings

import numpy as np
import pandas as pd
from joblib import Parallel, delayed

from maiagomes.data.utils import closest_friday, get_days_until_friday
from maiagomes.models.mmar.montecarlo import MMAR_simulator_handler
from maiagomes.models.options.pricing import Option_pricing_simulator

from .utils import parse_arguments


def get_option_contract_assessment_per_ticker(
    mmar_results: dict,
    ticker_symbol,
    minutes_until_friday: int,
    expiration_date: str,
    n_paths: int = 1000,
    min_thr: int = 390,
    paths_file: str = None,
    assessment_file: str = None,
    rank_max: int = None,
    persistent: bool = True,
    parallel: bool = True,
):
    logging.info(f"Starting assessment for {ticker_symbol}")
    try:
        simulator = MMAR_simulator_handler(
            mmar_results,
            T=minutes_until_friday,
        )
        simulator.prep(n_paths=n_paths, parallel=parallel)
        simulator.simulator.save_MMAR(paths_file)
        ops = Option_pricing_simulator(simulator=simulator)
        ops.define(ticker_symbol=ticker_symbol, expiration_date=str(expiration_date))
        with warnings.catch_warnings():
            warnings.filterwarnings("ignore", category=RuntimeWarning)
            assessment = ops.assess_all_contracts(
                showProgress=True,
                rank_max=rank_max,
                try_inTheMoney=False,
                min_thr=min_thr,
                parallel=parallel,
            )
            assessment = (
                pd.DataFrame(assessment)
                .T.reset_index()
                .rename(columns={"index": "contractSymbol"})
            )
        assessment.to_csv(assessment_file, index=False)
        return assessment_file
    except Exception as err:
        logging.error(err)
        if persistent:
            None


def get_option_contract_assessment(
    mmar_results: dict,
    minutes_until_friday: int,
    expiration_date: str,
    paths_folder: str,
    assessment_folder: str,
    n_paths: int = 1000,
    min_thr: int = 390,
    rank_max: int = None,
    persistent: bool = True,
    parallel: bool = True,
):
    if parallel:
        n_cores = multiprocessing.cpu_count() - 1
        _ans_ = Parallel(n_jobs=n_cores)(
            delayed(get_option_contract_assessment_per_ticker)(
                mmar_result,
                ticker_symbol,
                minutes_until_friday,
                expiration_date,
                n_paths=n_paths,
                paths_file=os.path.join(paths_folder, f"{ticker_symbol}.npy"),
                assessment_file=os.path.join(assessment_folder, f"{ticker_symbol}.csv"),
                rank_max=rank_max,
                min_thr=min_thr,
                persistent=persistent,
                parallel=False,
            )
            for ticker_symbol, mmar_result in mmar_results.items()
        )
        ans = dict(zip(mmar_results.keys(), _ans_))
    else:
        ans = {}
        for ticker_symbol, mmar_result in mmar_results.items():
            paths_file = os.path.join(paths_folder, f"{ticker_symbol}.npy")
            assessment_file = os.path.join(assessment_folder, f"{ticker_symbol}.csv")
            ans[ticker_symbol] = get_option_contract_assessment_per_ticker(
                mmar_result,
                ticker_symbol,
                minutes_until_friday,
                expiration_date,
                n_paths=n_paths,
                paths_file=paths_file,
                assessment_file=assessment_file,
                rank_max=rank_max,
                min_thr=min_thr,
                persistent=persistent,
                parallel=True,
            )
    _full_ = {}
    for ticker_symbol, assessment_file in ans.items():
        if assessment_file is not None:
            try:
                _full_[ticker_symbol] = pd.read_csv(assessment_file)
            except Exception as err:
                logging.error(err)
                continue

    _full_ = pd.concat(_full_)

    _full_.to_csv(os.path.join(assessment_folder, "_full_.csv"))


def option_contract_assessment_runner():
    args = parse_arguments(sys.argv[1:])
    logging.basicConfig(level=args.loglevel)
    mmar_results = json.load(
        open(
            args.mmar_results_file,
            "rb",
        )
    )
    days_until_friday = get_days_until_friday(weeks=args.weeks) - 1
    expiration_date = closest_friday(days_until_friday=days_until_friday + 1)
    logging.info(f"Expiration date: {expiration_date}.")
    week_days_until_friday = int(
        days_until_friday - 2 * np.floor(days_until_friday / 7)
    )
    minutes_until_friday = int(week_days_until_friday * 390)

    get_option_contract_assessment(
        mmar_results=mmar_results,
        minutes_until_friday=minutes_until_friday,
        expiration_date=str(expiration_date),
        paths_folder=args.paths_folder,
        assessment_folder=args.assessment_folder,
        n_paths=args.n_paths,
        min_thr=args.min_thr,
        rank_max=args.rank_max,
        persistent=args.persistent,
        parallel=args.parallel,
    )
