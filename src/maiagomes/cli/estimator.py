import datetime
import json
import logging
import multiprocessing
import os
import sys
from time import time

import pandas as pd
from dotenv import load_dotenv
from joblib import Parallel, delayed

from maiagomes.models.mmar.mmar import mmar

from .utils import parse_arguments

logging.basicConfig(level=logging.INFO)

load_dotenv()


def get_mmar_estimates_per_symbol(
    symbol, symbol_type="stock", cache_folder="../../data/np", persistent=True
):
    try:
        logging.info(f"Starting estimation for {symbol}")
        t0 = time()
        df = pd.read_csv(os.path.join(cache_folder, symbol_type, f"{symbol}.csv"))
        m_pt = mmar(df.close)
        m_pt.estimate(min_box_size=50)
        m_pt.param["N"] = m_pt.Pt.shape[0]
        logging.info(
            f"MMAR estimation for {symbol} completed in {(time()-t0)/60:.2f} minutes."
        )
        return m_pt.param
    except Exception as err:
        logging.error(err)
        if persistent:
            None


def get_mmar_estimates(
    target_symbols,
    mmar_results="../../results/mmar",
    symbol_type="stock",
    cache_folder="../../data/np",
    persistent=True,
    parallel=True,
):
    for symbol_type, symbols in target_symbols.items():
        if symbol_type == "crypto":
            continue
        if parallel:
            n_cores = multiprocessing.cpu_count() - 1
            _ans_ = Parallel(n_jobs=n_cores)(
                delayed(get_mmar_estimates_per_symbol)(
                    symbol,
                    symbol_type=symbol_type,
                    cache_folder=cache_folder,
                    persistent=persistent,
                )
                for symbol in symbols
            )
            ans = dict(zip(symbols, _ans_))
        else:
            ans = {}
            for symbol in symbols:
                ans[symbol] = get_mmar_estimates_per_symbol(
                    symbol,
                    symbol_type=symbol_type,
                    cache_folder=cache_folder,
                    persistent=persistent,
                )

    json.dump(ans, open(mmar_results, "w"))
    results_folder = os.path.dirname(mmar_results)
    json.dump(
        ans,
        open(
            os.path.join(
                results_folder,
                f"older/results_{str(datetime.datetime.now().date())}.json",
            ),
            "w",
        ),
    )


def mmar_estimator_runner():
    args = parse_arguments(sys.argv[1:])
    logging.basicConfig(level=args.loglevel)
    target_symbols = json.load(open(args.target_symbols, "r"))

    get_mmar_estimates(
        target_symbols=target_symbols,
        mmar_results=args.mmar_results_file,
        cache_folder=args.cache_folder,
        persistent=args.persistent,
        parallel=args.parallel,
    )
