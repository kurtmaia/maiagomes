import json
import logging
import os
import sys
from datetime import datetime

from alpaca.data.historical import CryptoHistoricalDataClient, StockHistoricalDataClient
from alpaca.trading.client import TradingClient
from dotenv import load_dotenv

from maiagomes.data.alpaca.pull import Pull_bars_from_alpaca
from maiagomes.investing.portfolio import OptionsTrader, RebalancePortfolio
from maiagomes.investing.portfolio.utils import is_market_open
from maiagomes.models.allocate import Allocate_arma, Allocate_mmar, Allocate_options

from .utils import parse_arguments

load_dotenv()


def get_client_type_sep(paper, paper_profile="paper"):
    if paper:
        client_type_sep = "_"
        if paper_profile != "paper":
            client_type_sep = f"_{paper_profile.upper()}_"
    else:
        client_type_sep = "_LIVE_"
    return client_type_sep


def rebalance_portfolio_using_mmar():
    args = parse_arguments(sys.argv[1:])
    logging.basicConfig(level=args.loglevel)
    portfolio_weights = Allocate_mmar(
        mmar_results_file=args.mmar_results_file,
        remap={"FB": "META"},
        blacklisted_symbols=["BNB", "XRP", "ADA", "PBR-A"],
    ).allocate()
    trading_client = TradingClient(
        args.api_key_id, args.api_secret_key, paper=args.paper
    )
    if not is_market_open(args.api_key_id, args.api_secret_key, paper=args.paper):
        logging.error("Market is closed. Exiting.")
        return None
    rebalancer = RebalancePortfolio(trading_client, portfolio_weights=portfolio_weights)
    rebalancer.prep_orders()
    rebalancer.run()


def rebalance_portfolio_using_arma():
    args = parse_arguments(sys.argv[1:])
    logging.basicConfig(level=args.loglevel)
    client_type_sep = get_client_type_sep(args.paper, args.paper_profile)
    if not is_market_open(
        os.getenv(f"APCA{client_type_sep}API_KEY_ID"),
        os.getenv(f"APCA{client_type_sep}API_SECRET_KEY"),
        paper=args.paper,
    ):
        logging.error("Market is closed. Exiting.")
        return None
    symbols = json.load(open(args.target_symbols, "rb"))
    stock_client = StockHistoricalDataClient(
        os.getenv(f"APCA{client_type_sep}API_KEY_ID"),
        os.getenv(f"APCA{client_type_sep}API_SECRET_KEY"),
    )
    crypto_client = CryptoHistoricalDataClient()
    latest_values = Pull_bars_from_alpaca(
        stock_client, crypto_client, start=datetime(2023, 1, 1)
    ).pull(symbols["crypto"] + symbols["stock"])
    portfolio_weights = Allocate_arma(
        symbols=list(latest_values.columns[1:]),
        remap={"FB": "META"},
        blacklisted_symbols=["BNB", "XRP", "ADA", "PBR-A"],
    ).allocate(
        daily_df=latest_values.set_index("timestamp"), p=args.p, d=args.d, q=args.q
    )
    trading_client = TradingClient(
        os.getenv(f"APCA{client_type_sep}API_KEY_ID"),
        os.getenv(f"APCA{client_type_sep}API_SECRET_KEY"),
        paper=args.paper,
    )
    rebalancer = RebalancePortfolio(trading_client, portfolio_weights=portfolio_weights)
    rebalancer.prep_orders(
        total_tradable_money=args.total_tradable_money,
        percent_equity_to_trade=args.percent_equity_to_trade,
    )
    rebalancer.run()
    return rebalancer.orders["status"]


def options_trading():
    args = parse_arguments(sys.argv[1:])
    logging.basicConfig(level=args.loglevel)
    client_type_sep = get_client_type_sep(args.paper, args.paper_profile)

    if not is_market_open(
        os.getenv(f"APCA{client_type_sep}API_KEY_ID"),
        os.getenv(f"APCA{client_type_sep}API_SECRET_KEY"),
        paper=args.paper,
    ):
        logging.error("Market is closed. Exiting.")
        return None
    symbols = json.load(open(args.target_symbols, "rb"))
    stock_client = StockHistoricalDataClient(
        os.getenv(f"APCA{client_type_sep}API_KEY_ID"),
        os.getenv(f"APCA{client_type_sep}API_SECRET_KEY"),
    )
    crypto_client = CryptoHistoricalDataClient()
    latest_values = Pull_bars_from_alpaca(
        stock_client, crypto_client, start=datetime(2023, 1, 1)
    ).pull(symbols=symbols["stock"])
    allocation_weights = Allocate_options(
        args.options_assessment_file,
        latest_values.set_index("timestamp"),
        min_cost=args.min_cost,
        max_cost=args.max_cost,
        univariate_allocation_variable=args.univariate_allocation_variable,
    ).allocate()
    trading_client = TradingClient(
        os.getenv(f"APCA{client_type_sep}API_KEY_ID"),
        os.getenv(f"APCA{client_type_sep}API_SECRET_KEY"),
        paper=args.paper,
    )
    trader = OptionsTrader(trading_client, allocation_weights)
    trader.trade(
        rebalance=args.rebalance_options,
        total_tradable_money=args.total_tradable_money,
        percent_equity_to_trade=args.percent_equity_to_trade,
        max_option_investment_rate=args.percent_equity_to_trade,
    )
    return None
