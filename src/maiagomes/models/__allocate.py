import json
import logging
import warnings
from dataclasses import dataclass
from datetime import datetime
from itertools import product
from typing import Callable, Dict, Iterator, List, Optional, Union

from statsmodels.tools.sm_exceptions import ConvergenceWarning

warnings.simplefilter("ignore", ConvergenceWarning)


import numpy as np
import pandas as pd
import statsmodels.api as sm
from scipy.stats import dirichlet, multinomial, norm

from maiagomes.testing import Data_handler

logger = logging.getLogger(__name__)


def do_nothing(__input__):
    return __input__


class Allocate_w(object):
    def __init__(self, strategies):
        self.strategies = strategies

    def get(self, strategy, *args, **kwargs):
        return self.strategies[strategy](*args, **kwargs)


@dataclass
class Allocate_mmar:
    """Estimate mmar allocation"""

    def __init__(
        self,
        mmar_results_file: str = None,
        mmar_results: dict = None,
        remap: dict = None,
        whitelisted_symbols: List = None,
        blacklisted_symbols: List = None,
    ):
        if mmar_results_file is not None:
            mmar_results = json.load(open(mmar_results_file, "rb"))
        if remap is not None:
            for k, v in remap.items():
                mmar_results[v] = mmar_results[k]
                del mmar_results[k]
        if whitelisted_symbols is not None:
            mmar_results = {
                k: v for k, v in mmar_results.items() if k in whitelisted_symbols
            }
        elif blacklisted_symbols is not None:
            mmar_results = {
                k: v for k, v in mmar_results.items() if k not in blacklisted_symbols
            }
        self.mmar_results = pd.DataFrame(mmar_results).T

    def simple(
        self,
        mmar_results,
        buying_power=1,
        full_output=False,
        wrapper_function=do_nothing,
    ):
        df = mmar_results.copy()
        df["norm_mu"] = df["mu"] / np.sqrt(df["sigma2"] / df["N"])
        p = df["norm_mu"].values
        p /= p.sum()
        df["alloc"] = wrapper_function(p * buying_power)
        if full_output:
            return df
        else:
            return df.alloc.to_dict()

    def allocate(self, strategies: dict = None, **kwargs):
        if strategies is None:
            strategies = {"mmar_alloc": self.simple}
        allocator = Allocate_w(strategies)
        return allocator.get("mmar_alloc", mmar_results=self.mmar_results, **kwargs)


class Allocate_arma:
    """Estimate arma allocation"""

    def __init__(
        self,
        symbols: list = None,
        time_series_df_path: str = None,
        time_series_df: pd.DataFrame = None,
        remap: dict = None,
        whitelisted_symbols: List = [],
        blacklisted_symbols: List = [],
        add_cash: bool = False,
        filter_out: str = "Date > '2023-01-01'",
    ):
        if symbols is None:
            time_series_df = pd.read_csv(time_series_df_path)
            if remap is not None:
                time_series_df = time_series_df.rename(columns=remap)
            self.data_h = Data_handler(
                df=time_series_df,
                date_var="Date",
                add_cash=add_cash,
                filter_out=filter_out,
            )

            if len(whitelisted_symbols) == 0:
                self.symbols = list(
                    set(self.data_h.symbolList) - set(blacklisted_symbols)
                )
            else:
                self.symbols = whitelisted_symbols
        else:
            self.symbols = symbols

    def simple(self, daily_df: pd.DataFrame = None, **kwargs):
        symbols = self.symbols

        if daily_df is None:
            daily_df = self.data_h.get_last_events_by("D").reset_index(drop=True)
        else:
            symbols = daily_df.columns
        expected_returns = np.zeros(len(symbols))
        for i, symb in enumerate(symbols):
            expected_returns[i] = get_arma_win_probability(
                np.log(daily_df[symb].values), **kwargs
            )[0]
        _expected_returns_ = np.nan_to_num(expected_returns, nan=0)
        _expected_returns_ = _expected_returns_ * (_expected_returns_ > 0)
        lnmat = np.log(daily_df.iloc[1:,]).diff(axis=0)[1:]
        lnmat_discretized = -1 * (lnmat < 0) + (lnmat > 0)
        weighted_graph = lnmat_discretized.T.dot(lnmat_discretized)
        weighted_graph_sum = (1.0 * (lnmat_discretized != 0)).T.dot(
            1.0 * (lnmat_discretized != 0)
        )
        _rho_ = np.nan_to_num(abs(weighted_graph / weighted_graph_sum))
        # _rho_ = np.nan_to_num(abs(np.corrcoef(lnmat.T)))
        _expected_returns_ = multivariate_kelly_allocation(
            _expected_returns_, theta=_rho_
        )
        if _expected_returns_.sum() > 1:
            logger.warning(
                f"Allocation suggests leveraging: {_expected_returns_.sum()} > 1 "
            )
            _expected_returns_ /= _expected_returns_.sum()
        elif _expected_returns_.sum() < 1:
            logger.warning(
                f"Allocation suggests saving {1- _expected_returns_.sum():.3%} in cash."
            )
        ans = dict(zip(symbols, list(_expected_returns_)))
        ans["_CASH_"] = 1 - _expected_returns_.sum()
        return ans

    def allocate(self, strategies: dict = None, **kwargs):
        if strategies is None:
            strategies = {"arma": self.simple}
        allocator = Allocate_w(strategies)
        return allocator.get("arma", **kwargs)


def get_random_portfolio(historyPrices, currDate):
    n_basket = historyPrices.shape[1]
    p = np.array([1.0 / n_basket] * n_basket)
    return (multinomial.rvs(n=n_basket, p=p) / n_basket).reshape([1, -1])


def get_simple_portfolio(historyPrices, currDate):
    n_basket = historyPrices.shape[1]
    return np.array([1.0 / n_basket] * n_basket).reshape([1, -1])


def get_arma_prediction(ts, p=-1, d=1, q=0, steps=1, **kwargs):
    if p == -1 or q == -1:
        order = auto_tune_arima(ts, **kwargs)
    else:
        order = (p, d, q)
    model = sm.tsa.ARIMA(ts, order=order)
    results = model.fit()

    forecast_values = results.get_forecast(steps=steps)
    return forecast_values


def get_arma_expected_return(ts, stdize=False, **kwargs):
    preds = get_arma_prediction(ts, **kwargs)
    ans = np.log(preds.predicted_mean) - np.log(ts[-1])
    if stdize:
        ans += -np.log(preds.se_mean)
    return ans


def get_kelly_c(p, a=1.0, b=1.0):
    if b < 0:
        _a_ = np.max([a, abs(b)])
        _b_ = 1
    else:
        _a_ = a
        _b_ = b
    return p / _a_ - (1 - p) / _b_


def get_arma_win_probability(ts, **kwargs):
    preds = get_arma_prediction(ts, **kwargs)
    diff = preds.predicted_mean - ts[-1]
    p = norm.cdf(diff / preds.se_mean)
    return get_kelly_c(p, b=np.exp(diff)[0])


def multivariate_kelly_allocation(
    univariate_kelly_allocation, theta=None, memberships=None, node_idx=None
):
    if theta is None:
        theta = np.eye(len(univariate_kelly_allocation))
    if memberships is None:
        memberships = np.eye(len(univariate_kelly_allocation))
    _rho_ = memberships.dot(theta).dot(memberships.T)
    final_allocation = np.zeros(univariate_kelly_allocation.shape)
    if node_idx is None:
        node_idx = univariate_kelly_allocation.argmax()
    dish_idx = memberships[node_idx,].argmax()
    alloc = univariate_kelly_allocation[node_idx]
    final_allocation[node_idx] = alloc
    while alloc > 0.01 and alloc <= 1:
        theta[:, dish_idx] = 1.0
        _rho_ = memberships.dot(theta).dot(memberships.T)
        univariate_kelly_allocation = ((1 - alloc) * univariate_kelly_allocation) * (
            1 - _rho_[node_idx,]
        )
        node_idx = univariate_kelly_allocation.argmax()
        dish_idx = memberships[node_idx,].argmax()
        alloc = univariate_kelly_allocation[node_idx]
        final_allocation[node_idx] = alloc

    return final_allocation


def get_arma_portfolio(historyPrices, currDate, past_size=10, p=1, d=1, q=-1, **kwargs):
    if historyPrices.shape[0] <= past_size:
        return get_simple_portfolio(historyPrices, currDate)
    expected_returns = np.zeros(historyPrices.shape[1])
    for i in range(historyPrices.shape[1] - 1):
        expected_returns[i] = get_arma_win_probability(
            np.log(historyPrices[:, i]), p=p, d=d, q=q, **kwargs
        )[0]
    _expected_returns_ = np.nan_to_num(expected_returns, nan=0)
    _expected_returns_ = _expected_returns_ * (_expected_returns_ > 0)
    lnmat = np.diff(np.log(historyPrices), axis=0)
    lnmat_discretized = -1 * (lnmat < 0) + (lnmat > 0)
    weighted_graph = lnmat_discretized.T.dot(lnmat_discretized)
    weighted_graph_sum = (1.0 * (lnmat_discretized != 0)).T.dot(
        1.0 * (lnmat_discretized != 0)
    )
    _rho_ = np.nan_to_num(abs(weighted_graph / weighted_graph_sum))
    # _rho_ = np.nan_to_num(abs(np.corrcoef(lnmat.T)))
    _expected_returns_ = multivariate_kelly_allocation(_expected_returns_, _rho_)
    if _expected_returns_.sum() > 1:
        logger.warning(
            f"Allocation suggests leveraging: {_expected_returns_.sum()} > 1 "
        )
        _expected_returns_ /= _expected_returns_.sum()
    elif _expected_returns_.sum() < 1:
        logger.warning(
            f"Allocation suggests saving {1- _expected_returns_.sum():.3%} in cash."
        )
        _expected_returns_[-1] = 1 - _expected_returns_.sum()
    return _expected_returns_.reshape([1, -1])


def get_mmar_portfolio(
    historyPrices,
    currDate,
    mmar_results=None,
    min_date=datetime(2021, 11, 2, 0, 0),
    symbolList=None,
):
    latest_results_date = currDate.astype("datetime64[D]")
    results_do_not_exist = str(latest_results_date) not in mmar_results.keys()
    while results_do_not_exist:
        latest_results_date -= 1
        if datetime.strptime(str(latest_results_date), "%Y-%m-%d") < min_date:
            return get_simple_portfolio(historyPrices, currDate)
        else:
            results_do_not_exist = str(latest_results_date) not in mmar_results.keys()
    currEstimates = mmar_results[str(latest_results_date)]
    df = pd.DataFrame(currEstimates).T
    df["norm_mu"] = df["mu"] / np.sqrt(df["sigma2"] / df["N"])
    p = dirichlet.rvs(df.norm_mu.values)[0]
    df["alloc"] = p
    allocs = df.alloc.to_dict()
    ans = np.zeros(len(symbolList))
    for i, sym in enumerate(symbolList):
        if sym in allocs.keys():
            ans[i] = allocs[sym]
    ans /= ans.sum()
    return ans


def auto_tune_arima(
    ts, p_values=range(0, 3), d_values=range(0, 2), q_values=range(0, 3)
):
    split_point = int(len(ts) * 0.9)  # Adjust the split percentage as needed

    # Split the time series into training and testing sets
    train_data = ts[:split_point]
    test_data = ts[split_point:]
    best_mse = float("inf")
    best_order = None

    for p, d, q in product(p_values, d_values, q_values):
        model = sm.tsa.ARIMA(train_data, order=(p, d, q))
        try:
            results = model.fit()
            predicted = results.get_forecast(steps=len(test_data))
            error = predicted.predicted_mean - test_data
            mse = (error**2).sum()
            # aic = results.aic
            if mse < best_mse:
                best_mse = mse
                best_order = (p, d, q)
        except:
            continue
    return best_order
