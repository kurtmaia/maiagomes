import datetime

import numpy as np

from .estimate import *
from .montecarlo import *
from .utils import *


class mmar(object):
    def __init__(self, Pt):
        self.Pt = Pt
        self.Pt_hat = None

    def estimate(self, create_df=False, **kwargs):
        param = mmar_inference(self.Pt)
        param.fit(create_df=create_df, **kwargs)
        nn = ["H", "alpha0", "mu", "sigma2"]
        vals = [param.H_, param.alpha0, param.lambda_, param.sigma2]
        param = dict(zip(nn, vals))
        self.param = param

    def sample(
        self, param=None, nSamples=1000, T=None, base=2, power=8, diff=True, **kwargs
    ):
        if T is not None:
            power = int(np.ceil(np.log(T) / np.log(base)))
        self.nSamples = nSamples
        self.base = base
        self.power = power
        if param is None:
            param = self.param
        simulated_data = mc_mmar(self.base, self.power, **param)
        simulated_data.gen(mcSamples=self.nSamples, diff=diff, **kwargs)
        mc_ = simulated_data.mmar_df.query("variable == 'mmar'").copy()
        mc_["Xt"] = mc_.value.values
        if diff:
            mc_.loc[mc_.query("t==0").index, ["value"]] = self.Pt[-1]
            mc_["Pt"] = mc_.groupby("mcSample")["value"].transform(pd.Series.cumsum)
        else:
            mc_["Pt"] = mc_.value + self.Pt[-1]
        mc_ = mc_.drop(["value"], axis=1)
        self.Pt_hat = mc_.copy()
        if T is not None:
            self.Pt_hat = self.Pt_hat.query(f"t <= {T}")

    def remove_debt_path(self, mc_=None):
        assert self.Pt_hat is not None, "There is no MC samples."
        mc_ = self.Pt_hat.copy()
        mc_["tmp"] = mc_["Pt"] > 0
        mc_["tmp"] = mc_.groupby("mcSample")["tmp"].transform(pd.Series.cumsum) - 1
        mc_["Pt"] = mc_.Pt * (mc_.t == mc_.tmp)
        mc_["Xt"] = mc_.Xt * (mc_.t == mc_.tmp)
        mc_ = mc_.drop(["tmp"], axis=1)
        return mc_

    def series2matrix(self, serie="Pt"):
        assert self.Pt_hat is not None, "There is no MC samples."
        return self.Pt_hat.pivot(index="mcSample", columns="t", values=serie).values


class conditional_mmar(mmar):
    def __init__(self, mmar, Pt_obs, auto_tune=False, **kwargs):
        # self.Pt_hat = mmar.Pt_hat
        self.Pt_obs = Pt_obs.reshape([1, -1])
        startingPrice = Pt_obs[0]
        self.Xt_m = mmar.series2matrix("Xt")
        self.Xt0 = np.hstack([np.zeros([self.Xt_m.shape[0], 1]), self.Xt_m])
        self.Pt_m = startingPrice + np.cumsum(self.Xt0, axis=1)
        self.size = self.Pt_m.shape[0]
        self.path_list = None
        if auto_tune:
            self.auto_tune(**kwargs)

    def auto_tune(
        self,
        w_type=None,
        topN=None,
        obsTime=None,
        step_cv=20,
        step_bootstrap=60,
        get_path_list=False,
    ):
        if obsTime is None:
            obsTime = min(self.Pt_obs.shape[1], self.Pt_m.shape[1]) - step_bootstrap
        if w_type is None:
            cv = auto_tune_using_cv(
                self.Pt_m, self.Pt_obs, self.Xt0, obsTime=obsTime, step=step_cv
            )
            w_type = "w{}".format(
                cv.groupby("weight")["performance"].sum().values.argmin()
            )
            self.w_type = w_type
        else:
            self.w_type = w_type
        if topN is None:
            btt = auto_tune_using_bootstrap(
                self.Pt_m,
                self.Pt_obs,
                self.Xt0,
                obsTime=obsTime,
                step=step_bootstrap,
                types=[self.w_type],
            )
            topN = np.average(btt["topN"].values, weights=1 / btt["performance"].values)
            self.topN = 50 if topN < 50 else int(topN.round())
        else:
            self.topN = topN
        if get_path_list:
            self.get_path_list(obsTime=obsTime)

    def get_path_list(self, obsTime=None):
        if obsTime is None:
            obsTime = min(self.Pt_obs.shape[1], self.Pt_m.shape[1])
        path_list = get_weighted_euclidean_distances(
            self.Pt_m, self.Pt_obs, obsTime=obsTime, types=[self.w_type]
        )[self.w_type].argsort()
        self.path_list = path_list[: self.topN]

    def paths(self, obsTime, path_list=None, topN=None, w_type=None):
        if path_list is None:
            if self.path_list is None:
                self.get_path_list()
            path_list = self.path_list
        Pt_m = self.Pt_obs[0, obsTime] + np.cumsum(
            self.Xt0[path_list, (obsTime + 1) :], axis=1
        )
        # Pt_m = self.Pt_m[path_list,]
        adf = pd.DataFrame(Pt_m.T)
        adf["t"] = adf.index.values + obsTime
        ans = pd.DataFrame(adf).melt(
            id_vars=["t"], var_name="mcSample", value_name="Pt"
        )
        self.cond_Pt_hat = ans
        return Pt_m
