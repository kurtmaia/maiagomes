import multiprocessing

import numpy as np
import pandas as pd
from fbm import FBM
from joblib import Parallel, delayed
from scipy.stats import norm
from tqdm import tqdm

from .fGn import *
from .utils import create_an_sparse_indexer, lognormal_cascade, multi_lognormal_cascade, ecdf_py


class MMAR_montecarlo(object):
    def __init__(self, b, k, H, mu, sigma):
        assert H > 0 and H <= 2, "The Hurst parameter must be in the interval (0,2]."
        assert sigma > 0, "Standard deviation must be greater than zero."

        self.b = int(b)
        self.k = int(k)
        self.H = H
        self.mu = mu
        self.sigma = sigma
        self.T = int(b**k)
        self.M0 = 1.0

    def sample_single_fbm_mmar(
        self, sample_number, _lower_, _upper_, useFBM=True, diff=False, **kwargs
    ):
        if useFBM:
            _B_ = FBM(self.b**self.k - 1, self.H).fbm()
        else:
            _B_ = fBm(self.H, T=self.T, N=self.T).fast_gen(**kwargs)
        _MMAR_ = np.multiply(
            self.cdf_V[:, sample_number],
            np.matrix(_B_) * create_an_sparse_indexer(_lower_[:, sample_number] - 1),
        ) + np.multiply(
            1 - self.cdf_V[:, sample_number],
            np.matrix(_B_) * create_an_sparse_indexer(_upper_[:, sample_number] - 1),
        )
        # self.B[:, sample_number] = _B_
        _MMAR_ = np.array(_MMAR_)[0, :]
        if diff:
            _MMAR_ = np.hstack([0, np.diff(_MMAR_)])
        return _B_, _MMAR_

    def gen(
        self,
        mcSamples=1,
        diff=False,
        useFBM=True,
        progress_bar=False,
        parallel=False,
        **kwargs
    ):

        V, cdf_V = multi_lognormal_cascade(
            self.b, self.k, self.M0, self.mu, self.sigma, nsamples=mcSamples
        )
        cdf_V /= cdf_V[-1, :]
        self.cdf_V = cdf_V
        _index_ = cdf_V * self.T

        _lower_ = np.floor(_index_).astype(int)
        _lower_ = _lower_ * (_lower_ > 1) + (_lower_ <= 1)

        _upper_ = np.ceil(_index_).astype(int)
        _upper_ = _upper_ * (_upper_ < self.T) + self.T * (_upper_ >= self.T)

        self.MMAR = np.zeros(_index_.shape)
        self.B = np.zeros(_index_.shape)
        if parallel:
            n_cores = multiprocessing.cpu_count() - 1
            _output_ = Parallel(n_jobs=n_cores)(
                delayed(self.sample_single_fbm_mmar)(
                    mc, _lower_, _upper_, useFBM=useFBM, diff=diff
                )
                for mc in range(mcSamples)
            )
            for mc, _o_ in enumerate(_output_):
                self.B[:, mc], self.MMAR[:, mc] = _o_
        else:
            for mc in tqdm(range(mcSamples), disable=not progress_bar):
                self.B[:, mc], self.MMAR[:, mc] = self.sample_single_fbm_mmar(
                    mc, _lower_, _upper_, useFBM=useFBM, diff=diff
                )

    def get_dict(self):
        assert hasattr(self, "MMAR"), "First generate the montecarlo paths"
        mmar_dict = {}
        for mc in range(self.MMAR.shape[1]):
            mmar_dict[mc] = {
                "t": [t for t in range(self.T)],
                "mmar": self.MMAR[:, mc],
                "B": self.B[:, mc],
                "cdf": self.cdf_V[:, mc],
            }
        self.mmar_dict = mmar_dict

    def df(self):
        if not hasattr(self, "mmar_dict"):
            self.get_dict()
        df = []
        for k, v in self.mmar_dict.items():
            _tmp_ = pd.DataFrame(v)
            _tmp_["mcSample"] = k
            df.append(_tmp_)
        return pd.concat(df)

    def save_MMAR(self, file: str = None):
        np.save(file, self.MMAR)


class mc_mmar(object):
    def __init__(self, b, k, H, mu, sigma):
        assert H > 0 and H <= 2, "The Hurst parameter must be in the interval (0,2]."
        assert sigma > 0, "Standard deviation must be greater than zero."

        self.b = b
        self.k = k
        self.H = H
        self.mu = mu
        self.sigma = sigma
        self.T = b**k
        self.M0 = 1.0

    def gen(self, mcSamples=1, diff=True, useFBM=True, progress_bar=True, **kwargs):
        self.mmar_df = pd.DataFrame()
        for mc in tqdm(range(mcSamples), disable=not progress_bar):
            if useFBM:
                B = FBM(self.b**self.k - 1, self.H).fbm()
            else:
                B = fBm(self.H, T=self.T, N=self.T).fast_gen(**kwargs)

            V, cdf_V = lognormal_cascade(self.b, self.k, self.M0, self.mu, self.sigma)
            cdf_V /= cdf_V[-1]

            MMAR = np.zeros(self.T)
            for i in range(self.T):
                index = cdf_V[i] * self.T
                lower = np.floor(index) if index > 1 else 1
                upper = np.ceil(index) if index < self.T else self.T
                w2 = cdf_V[i]
                w1 = 1.0 - cdf_V[i]
                MMAR[i] = w1 * B[int(lower - 1)] + w2 * B[int(upper - 1)]

            if diff:
                mmar = np.hstack([0, np.diff(MMAR)])
            else:
                mmar = MMAR
            self.mmar_dict = {
                "mcSample": mc,
                "t": [t for t in range(self.T)],
                "mmar": mmar,
                "B": B,
                "cdf": cdf_V,
            }
            self.mmar_df = pd.concat(
                [
                    self.mmar_df,
                    pd.DataFrame(self.mmar_dict).melt(id_vars=["mcSample", "t"]),
                ],
                ignore_index=True,
            )
        return self.mmar_df


class MMAR_simulator_handler:
    def __init__(
        self,
        mmar_params: dict = None,
        simulator: MMAR_montecarlo = None,
        samples=None,
        T=None,
        base=2,
        expoent=10,
    ):
        if T is not None:
            expoent = np.ceil(np.log(T) / np.log(base))
            self.T = T
        else:
            self.T = base**expoent
        if simulator is not None:
            self.simulator = simulator
        else:
            self.simulator = MMAR_montecarlo(
                base,
                expoent,
                mu=mmar_params["mu"],
                H=mmar_params["H"],
                sigma=np.sqrt(mmar_params["sigma2"]),
            )
        self.samples = samples

    def prep(self, force=False, **kwargs):
        if self.samples is None or force:
            self.get_paths(**kwargs)
        self.get_matrix()

    def get_paths(self, n_paths=1000, **kwargs):
        self.simulator.gen(n_paths, diff=False, **kwargs)

    def get_samples(self):
        self.samples = self.simulator.df().set_index(["variable"])

    def get_matrix(self):
        if hasattr(self.simulator, "MMAR"):
            self.mat = self.simulator.MMAR[:, : self.T]
        else:
            self.mat = (
                self.samples.loc["mmar"]
                .pivot(index="mcSample", columns="t", values="value")
                .values
            )[:, : self.T]
    
    def get_ecdf_by_path(self):
        return ecdf_py(self.mat, axis = 0)

    def get_option_price_paths(
        self,
        S,  # Current stock price
        K,  # Option strike price
        sigma,  # Option strike price
        r=0.05,  # Risk-free interest rate
        minutes_until_expiration: int = None,
        direction: str = None,
        adjust_trading_time: bool = True,
        **kwargs
    ):
        if not hasattr(self, "mat"):
            self.prep()
        mat = self.mat
        if minutes_until_expiration is None:
            minutes_until_expiration = mat.shape[1]
        if adjust_trading_time:
            minutes_to_expiration_array = (
                24 * 60 * np.floor(np.arange(mat.shape[1]) / (60 * 6.5))
            )
        else:
            minutes_to_expiration_array = np.arange(mat.shape[1])
        T_years = (minutes_until_expiration - minutes_to_expiration_array) / (
            365 * 24 * 60
        )
        d1 = (np.log(S) + mat - np.log(K) + T_years * (r + 0.5 * sigma**2)) / (
            sigma * np.sqrt(T_years)
        )
        d2 = d1 - sigma * np.sqrt(T_years)

        if direction == "call" or direction == "both":
            call = np.exp(np.log(S) + mat + np.log(norm.cdf(d1))) - K * np.exp(
                -r * T_years
            ) * norm.cdf(d2)
            call = call * ((call > 0).cumprod(axis=1))
            if direction != "both":
                return call
        elif direction == "put" or direction == "both":
            put = K * np.exp(-r * T_years) * norm.cdf(-d2) - np.exp(
                np.log(S) + mat + np.log(norm.cdf(-d1))
            )
            put = put * ((put > 0).cumprod(axis=1))
            if direction != "both":
                return put

        return call, put

    def get_path_probability(
        self,
        log_return,
        mat=None,
        call_mode=True,
        min_thr=0,
        expiration_only=False,
        Teff=None,
    ):
        if mat is None:
            if not hasattr(self, "mat"):
                self.prep()
            mat = self.mat

        if expiration_only:
            if call_mode:
                return (mat[:, -1] >= log_return).mean()
            else:
                return (mat[:, -1] <= log_return).mean()
        if Teff is None:
            T = mat.shape[1]
            Teff = np.exp(-np.arange(T) / T)
        
        if call_mode:
            comp = Teff * (mat >= log_return)
        else:
            comp = Teff * (mat <= log_return)

        ans = comp.sum(axis=0) > min_thr
        return ans.mean()
