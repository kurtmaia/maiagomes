import csv

import numpy as np
import pandas as pd
import requests
from scipy.sparse import csr_matrix
from scipy.special import softmax
from tqdm import tqdm


def create_an_sparse_indexer(row_indices):
    num_rows = row_indices.shape[0]
    # Create a list of column indices, all set to 0
    col_indices = np.arange(num_rows)
    num_cols = col_indices.shape[0]

    # Create a list of values, all set to 1
    data = np.ones(num_rows)

    # Create the sparse matrix in CSR format
    sparse_matrix = csr_matrix(
        (data, (row_indices, col_indices)), shape=(num_rows, num_cols)
    )
    return sparse_matrix


def log_b(x, b=10):
    return np.log(x) / np.log(b)


def log2(x):
    return log_b(x, 2)


def gen_lognormal_b(mu, sigma, b=10, **kwargs):
    return np.random.lognormal(**kwargs) / np.log(b)


def lognormal_cascade(b, k, v, ln_lambda, ln_theta):
    for _ in range(k):
        M = np.random.lognormal(ln_lambda, ln_theta, size=b)
        v = np.kron(M, v)
    cdf_v = np.cumsum(v)
    return v, cdf_v


def multi_lognormal_cascade(b, k, v, ln_lambda, ln_theta, nsamples=1):
    V = np.zeros([int(b**k), nsamples])
    cdf_V = np.zeros([int(b**k), nsamples])
    v0 = v
    for i in range(nsamples):
        v = v0
        for _ in range(int(k)):
            M = np.random.lognormal(ln_lambda, ln_theta, size=b)
            v = np.kron(M, v)
        V[:, i] = v
        cdf_V[:, i] = np.cumsum(v)
    return V, cdf_V


def ecdf_py(x, axis=None, **kwargs):
    if axis is not None:
        return ecdf_py_axis(x, axis=axis, **kwargs)
    x = np.sort(x)

    def result(v):
        return np.searchsorted(x, v, side="right") / x.size

    return result


def ecdf_py_axis(data, axis=0, aggregate_function=None):
    """
    Compute the Empirical Cumulative Distribution Function (ECDF) along a specified axis.

    Parameters:
    - data (array-like): The input dataset.
    - axis (int): The axis along which to compute the ECDF.
    - aggregate_function (function): The function to aggregate the ECDF values across the other dimensions.

    Returns:
    - result (function): A function that computes the ECDF values for given inputs.
    """
    data = np.asarray(data)
    sorted_data = np.sort(data, axis=axis)
    data_size = data.shape[axis]

    def result(v):
        v = np.expand_dims(v, axis=axis)  # Match dimensions for broadcasting
        counts = np.sum(sorted_data <= v, axis=axis)
        if aggregate_function is not None:
            return aggregate_function(counts / data_size)
        return counts / data_size

    return result


def genW(n, types=["w0", "w1", "w2"]):
    ans = {}
    if "w0" in types:
        ans["w0"] = np.eye(n)
    if "w1" in types:
        w1 = np.diag(np.exp(np.arange(n) + 1))
        ans["w1"] = w1 / w1.sum()
    if "w2" in types:
        w2 = np.diag(softmax(np.arange(n)))
        ans["w2"] = w2 / w2.sum()
    return ans


def get_weighted_euclidean_distances(mm, oo, obsTime=None, **kwargs):
    if obsTime is None:
        obsTime = oo.shape[1]
    else:
        obsTime = min(obsTime, oo.shape[1])
    Ws = genW(obsTime, **kwargs)
    dist = mm[:, :obsTime] - oo[:, :obsTime]
    distTable = {}
    for wname, w in Ws.items():
        distTable[wname] = np.diag(dist.dot(w).dot(dist.T))
    return distTable


def auto_tune_using_bootstrap(
    pt, obs, xtm, step=None, obsTime=None, nBootstraps=100, types=["w1"], top_paths=10
):
    if obsTime is None:
        obsTime = min(obs.shape[1], pt.shape[1])
    if step is None:
        step = int(np.floor(obsTime / 5))
    exp_perfm = pd.DataFrame()
    nsamples = pt.shape[0]
    for bt in tqdm(range(nBootstraps)):
        ss = np.random.choice(nsamples, nsamples)
        ranges = np.arange(nsamples) + 1
        lastPrice = obs[0, (obsTime - step)]
        futurePrice = obs[0, obsTime + step]
        Pt_future = lastPrice + np.cumsum(xtm[ss,], axis=1)
        DD = get_weighted_euclidean_distances(
            pt[ss,], obs, obsTime=obsTime, types=types
        )
        for dname, d in DD.items():
            asort = d.argsort()
            perfm = (
                np.cumsum((Pt_future[asort, obsTime + step] - futurePrice) ** 2)
                / ranges
            )
            #   investigate future paths for the top from perfm
            topNs = perfm.argsort()[:top_paths] + 1
            perfm_ = np.zeros([top_paths])
            i = 0
            while i < top_paths:
                topN = topNs[i]
                perfm_[i] = get_weighted_euclidean_distances(
                    pt[asort[:topN], obsTime : (obsTime + step)],
                    obs[:, obsTime : (obsTime + step)],
                    types=[dname],
                )[dname].mean()
                i += 1
            exp_perfm_tmp = pd.DataFrame.from_dict(
                {
                    "bootstrap_sample": bt,
                    "weight": dname,
                    "performance": perfm_.min(),
                    "topN": topNs[perfm_.argmin()] + 1,
                },
                orient="index",
            )
            exp_perfm = exp_perfm.append(exp_perfm_tmp.T, ignore_index=True)
    return exp_perfm


def auto_tune_using_cv(
    pt, obs, xtm, nFolds=10, step=None, obsTime=None, top_paths=10, **kwargs
):
    if obsTime is None:
        obsTime = min(obs.shape[1], pt.shape[1])
    if step is None:
        step = int(np.floor(obsTime / (nFolds + 1)))
    size = pt.shape[0]
    ranges = np.arange(size) + 1
    exp_perfm = pd.DataFrame()
    for n in range(step, obsTime - step, step):
        lastPrice = obs[0, n]
        futurePrice = obs[0, n + step]
        Pt_future = lastPrice + np.cumsum(xtm, axis=1)
        DD = get_weighted_euclidean_distances(pt, obs, obsTime=n)
        for dname, d in DD.items():
            asort = d.argsort()
            perfm = np.cumsum((Pt_future[asort, n + step] - futurePrice) ** 2) / ranges
            #   investigate future paths for the top from perfm
            topNs = perfm.argsort()[:top_paths] + 1
            perfm_ = np.zeros([top_paths])
            i = 0
            while i < top_paths:
                topN = topNs[i]
                perfm_[i] = get_weighted_euclidean_distances(
                    pt[asort[:topN], n : (n + step)],
                    obs[:, n : (n + step)],
                    types=[dname],
                )[dname].mean()
                i += 1
            exp_perfm_tmp = pd.DataFrame.from_dict(
                {
                    "step": n,
                    "weight": dname,
                    "performance": perfm_.min(),
                    "topN": topNs[perfm_.argmin()] + 1,
                },
                orient="index",
            )
            exp_perfm = exp_perfm.append(exp_perfm_tmp.T, ignore_index=True)
    return exp_perfm


def moving_average(arr, window_size, axis=0):
    kernel = np.ones(window_size) / window_size
    return np.apply_along_axis(
        lambda m: np.convolve(m, kernel, mode="valid"), axis, arr
    )
