from scipy.stats import multivariate_normal, beta, norm
from numpy.linalg import inv, det, cholesky
from scipy.special import softmax
import numpy as np

from tqdm import tqdm

def lognormal_cascade(b, k, v, ln_lambda, ln_theta):
    for _ in range(k):
        M = np.random.lognormal(ln_lambda,ln_theta, size = b)
        v = np.kron(M,v)
    cdf_v = np.cumsum(v)    
    return v, cdf_v


def log_b(x,b = 10): 
  return np.log(x)/np.log(b)

def jump(xbar, var = .1*.07):
    var = xbar*(1-xbar)*var
    a = ((1-xbar)*xbar**2-var*xbar)/var
    a = 1 if a <= 0 else a
    b = a*(1-xbar)/xbar
    b = 1 if b <= 0 else b
    return np.random.beta(a,b,1), {'a':a,'b':b}

class inference(object):
    def __init__(self, base, power, Pt, v = 1):
         self.base = base
         self.power = power
         self.Xt = np.log(Pt) - np.log(Pt[0])
         self.T = Pt.shape[0]
         self.v = v


    def mmar_likelihood(self, H, alpha0):
        T = self.T
        mu = alpha0*1./H
        sigma2 = 2*(np.abs(mu - 1))/log_b(self.power, b = self.base)

        # v = self.v
        # for _ in range(self.power):
        #     M = np.repeat(mu, self.base)
        #     v = np.kron(M,v)
        # cdf_V = np.cumsum(v)
        _, cdf_V = lognormal_cascade(self.base,self.power,self.v,mu,np.sqrt(sigma2))
        cdf_V = cdf_V[:(T)]
        cdf_V = cdf_V/cdf_V[-1]

        theta_t = cdf_V.reshape([-1,1]).dot(np.ones([1,T]))
        theta_2h = theta_t**(2*H)
        cov_st = (1/2)*(theta_2h + theta_2h.T - (np.abs(theta_t-theta_t.T))**(2*H))
        # cov_st = cov_st - np.diag(np.diagonal(cov_st)) + np.eye(T)

        try:
            PH = inv(cholesky(cov_st))
            return norm.logpdf(PH.dot(self.Xt)).sum()
        except Exception as e:
            print(e)
            return multivariate_normal.logpdf(self.Xt, mean=np.zeros(T), cov=np.nan_to_num(cov_st), allow_singular = True)    


    def sample_H(self, params, hyperparms, var = None):
        if var is None:
            var = self.var
        currH = params['H']
        H_star, ab_star = jump(currH, var)
        log_p_H_star = self.mmar_likelihood(H = H_star, alpha0 = params['alpha0']) - beta.logpdf(H_star, **hyperparms)
        log_p_H_curr = self.mmar_likelihood(H = currH, alpha0 = params['alpha0']) - beta.logpdf(currH, **ab_star)
        probs = softmax(np.hstack([log_p_H_star,log_p_H_curr]))
        u = np.random.multinomial(1,np.nan_to_num(probs))[0]
        if u==1:
            H = H_star
            ab = ab_star
        else:
            H = currH 
            ab = hyperparms
        return H, ab, u


    def sample_alpha0(self, params, hyperparms, var = None):
        if var is None:
            var = self.var        
        curralpha0 = params['alpha0']
        alpha0_star, ab_star = jump(curralpha0, var)
        log_p_alpha0_star = self.mmar_likelihood(H = params['H'], alpha0 = alpha0_star) - beta.logpdf(alpha0_star, **hyperparms)
        log_p_alpha0_curr = self.mmar_likelihood(H = params['H'], alpha0 = curralpha0) - beta.logpdf(curralpha0, **ab_star)
        probs = softmax(np.hstack([log_p_alpha0_star,log_p_alpha0_curr]))
        u = np.random.multinomial(1,np.nan_to_num(probs))[0]
        if u==1:
            alpha0 = alpha0_star
            ab = ab_star
        else:
            alpha0 = curralpha0 
            ab = hyperparms
        return alpha0, ab, u


    def mcmc(self, H = .5, alpha0 = .5, nSamples = 1000, warmup = 100):
        params = {}
        hyperparms = {}
        u = {}
        params[0] = {'H': H, 'alpha0': alpha0}
        hyperparms[0] = {'H': {'a':1,'b':1},'alpha0': {'a':1,'b':1}}
        u[0] = {'H' : 0, 'alpha0' : 0}
        self.var = .1
        for iter in tqdm(range(warmup)):
            params[iter+1] = {}
            hyperparms[iter+1] = {}
            u[iter+1] = {}      
            params[iter+1]['H'], hyperparms[iter+1]['H'], u[iter+1]['H'] = self.sample_H(params[iter], hyperparms[iter]['H'])
            params[iter+1]['alpha0'], hyperparms[iter+1]['alpha0'], u[iter+1]['alpha0'] = self.sample_alpha0(params[iter], hyperparms[iter]['alpha0'])      
        acpt_H = np.mean([_["H"] for _ in u.values()])
        acpt_alpha0 = np.mean([_["alpha0"] for _ in u.values()])        
        for iter in tqdm(range(warmup,nSamples)):
            params[iter+1] = {}
            hyperparms[iter+1] = {}
            u[iter+1] = {}
            params[iter+1]['H'], hyperparms[iter+1]['H'], u[iter+1]['H'] = self.sample_H(params[iter], hyperparms[iter]['H'], var = self.var*acpt_H)
            acpt_H = (acpt_H*iter + u[iter+1]['H'])/(iter+1)
            params[iter+1]['alpha0'], hyperparms[iter+1]['alpha0'], u[iter+1]['alpha0'] = self.sample_alpha0(params[iter], hyperparms[iter]['alpha0'], var = self.var*acpt_alpha0)
            acpt_alpha0 = (acpt_alpha0*iter + u[iter+1]['alpha0'])/(iter+1)
        self.params = params
        self.hyperparms = hyperparms
        self.u = u
        return params




