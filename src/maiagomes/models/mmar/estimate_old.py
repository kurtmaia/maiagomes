import pandas as pd
import numpy as np
import statsmodels.formula.api as smf
import matplotlib.pyplot as plt
import seaborn as sns


from tqdm import tqdm
from mmar.utils import *

import logging


def log_b(x, b=10):
    return np.log(x) / np.log(b)


def sq_deltat(X, delta_t, q):
    TT = len(X)
    N = int(np.ceil(TT / delta_t))
    ans = 0.0
    for i in range(N):
        if (i + 1) * delta_t < TT:
            tmp = np.abs(X[(i + 1) * delta_t] - X[i * delta_t]) ** q
            ans += tmp
    return ans


def get_tau_q(sqa, TT):
    sqa["T"] = TT
    results = smf.ols(
        "log_b(Sq, b = 10) ~ log_b(dt, b = 10) + log_b(T, b = 10) - 1", data=sqa
    ).fit()
    tau_q = results.params.values[0]
    return tau_q


def f_alpha(alpha, q, tau_q):
    fa = q * alpha - tau_q
    qmin = q[np.argmin(fa)]
    return qmin, min(fa)


class mmar_inference:
    def __init__(self, Pt):
        logging.info("Preparing data...")
        self.Xt = np.log(Pt) - np.log(Pt[0])
        self.TT = len(self.Xt)

    def fit(
        self,
        max_deltat=1000,
        min_box_size=0,
        precision=0.01,
        progress_bar=True,
        print_results=False,
    ):
        assert precision <= 0.1, "precision needs to be at least 0.1 ."
        Xt = self.Xt
        TT = self.TT
        Sq = {}
        if max_deltat > TT:
            max_deltat = TT - 1
        if min_box_size > 0:
            max_deltat = int(np.ceil(TT / min_box_size))
            print("Fixing max_deltat to be {}".format(max_deltat))
        logging.info("Estimating Hurst parameter...")
        all_q = np.arange(0, 5, precision)
        for q in tqdm(all_q, disable=not progress_bar):
            # q = qq*precision
            Sq[q] = {}
            tmp = sq_deltat(Xt, 1, q)
            for deltat in range(1, max_deltat):
                Sq[q][deltat] = sq_deltat(Xt, deltat, q) / tmp
        Sq_df = pd.DataFrame.from_dict(Sq)
        Sq_df.reset_index(inplace=True)
        Sq_df_m = Sq_df.melt(id_vars="index")
        Sq_df_m.columns = ["dt", "q", "Sq"]
        Sq_df_m_byQ = Sq_df_m.groupby("q")
        tau_q_df = pd.DataFrame(Sq_df_m_byQ.apply(lambda q: get_tau_q(q, TT=len(Xt))))
        tau_q_df.columns = ["tau_q"]
        tau_qs = tau_q_df["tau_q"].values
        qs = tau_q_df.index.values
        q0 = qs[np.abs(tau_qs).argmin()]
        H_ = 1.0 / q0
        if print_results:
            print("H = {}".format(H_))
        logging.info("Estimating alpha0")
        f_alphas = {}
        for a in tqdm(np.arange(0, 2, precision), disable=not progress_bar):
            # a = aa*precision
            q, fa = f_alpha(a, qs, tau_qs)
            f_alphas[a] = fa
        alpha0 = list(f_alphas.keys())[
            ((np.array(list(f_alphas.values())) - 1.0) ** 2).argmin()
        ]
        if print_results:
            print("alpha0 = {}".format(alpha0))
        lambda_ = alpha0 / H_
        sigma2 = 2 * (np.abs(lambda_ - 1)) / log_b(2, b=10)
        logging.info("lambda = {}".format(lambda_))
        logging.info("sigma2 = {}".format(sigma2))

        self.min_box_size = min_box_size
        self.max_deltat = max_deltat
        self.H_ = H_
        self.alpha0 = alpha0
        self.lambda_ = lambda_
        self.sigma2 = sigma2
        qqs = [all_q[int(round(qq, 0))] for qq in np.linspace(1, len(all_q) - 1, 4)] + [
            q0
        ]
        self.Sq_df_m = Sq_df_m.query("q in {}".format(qqs))

        return H_, alpha0, lambda_, sigma2

    def plot(self, qqs=None):
        if qqs is not None:
            df = self.Sq_df_m.query("q in {}".format(qqs))
        else:
            df = self.Sq_df_m
        df["l_dt"] = np.log(df["dt"])
        df["l_Sq"] = np.log(df["Sq"])
        df["q"] = df["q"].astype("float")

        sns.scatterplot(x="l_dt", y="l_Sq", hue="q", data=df)
        plt.show()


# if __name__=='__main__':
# dmusd = pd.read_csv("~/OneDrive - purdue.edu/invest/dmusd_1973-06-01_1996-12-31.csv")
# print(estimate_mmar_params(dmusd.price.values))
