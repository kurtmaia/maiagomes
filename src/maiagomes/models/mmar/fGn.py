import numpy as np
import scipy.special as sce
from scipy.integrate import quad as integrate
from tqdm import tqdm

from .utils import *


def K_H(t, s, H=1):
    ans = (t - s) ** (H - 0.5) * (
        sce.hyp2f1(H - 0.5, 0.5 - H, H + 0.5, 1 - t / s) / sce.gamma(H + 0.5)
    )
    return ans


def B_H(j, ts, bm, N, T, H=1):
    int_ans = 0
    for i in range(j):
        int_ans += (integrate(lambda a: K_H(ts[j], a, H=H), ts[i], ts[i + 1])[0]) * bm[
            i
        ]
    return int_ans * (N / T)


class fBm(object):
    def __init__(self, H, sigma=1, mu=0, T=None, deltat=None, N=None):
        assert (
            T is None and N is None and deltat is None
        ) == False, (
            "T is None , N is None and deltat is None. Please specify two of them."
        )
        if T is None:
            T = N * deltat
        if N is None:
            N = int(T / deltat)

        assert N > 0, "Length of the return vector must be positive."

        assert H > 0 and H <= 2, "The Hurst parameter must be in the interval (0,2]."

        assert sigma > 0, "Standard deviation must be greater than zero."

        self.H = H
        self.T = T
        self.N = N
        self.sigma = sigma
        self.mu = mu
        self.ts = np.linspace(0, T, N + 1)
        self.deltat = T / N

    def gen(self, progress=None):
        if progress is None:
            progress = range(self.N)
        else:
            progress = tqdm(range(self.N))

        y = np.random.normal(size=self.N)
        bm = y * np.sqrt(self.T / self.N)
        fBm = np.zeros(self.N)

        for j in progress:
            fBm[j] = (
                B_H(j, self.ts, bm, self.N, self.T, H=self.H) * self.sigma + self.mu
            )

        return fBm

    def fast_gen(self, fBn=None):
        H = self.H
        T = self.T
        N = self.N
        sigma = self.sigma
        mu = self.mu
        deltat = self.deltat
        if fBn is None:
            if H <= 1:
                fBn = False
            else:
                H = H - 1
                fBn = True
        Nfft = 2 ** np.ceil(log2(2 * (N - 1)))
        NfftHalf = round(Nfft / 2)
        k = np.arange(NfftHalf)
        k = np.concatenate([k, k[::-1] + 1])
        Zmag = 0.5 * (
            (k + 1) ** (2.0 * H) - 2.0 * k ** (2.0 * H) + (np.abs(k - 1)) ** (2.0 * H)
        )
        del k
        Zmag = np.real(np.fft.fft(Zmag))
        if (Zmag < 0).any():
            raise ValueError(
                "The fast Fourier transform of the circulant covariance had negative values."
            )
        Zmag = np.sqrt(Zmag)
        Nlast = N
        Hlast = H
        Z = np.random.normal(size=int(Nfft)) + np.random.normal(size=int(Nfft)) * 1j
        y = np.real(np.fft.ifft(Z)) * np.sqrt(Nfft)
        del Z
        y = y[:N]

        y = y * sigma + mu
        if fBn:
            y = y.cumsum()
        return y


if __name__ == "__main__":
    print(gen_fBm(0.53, 0.59, 2, T=40, N=1000))
