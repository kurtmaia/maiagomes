import logging

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import statsmodels.formula.api as smf
from numpy.linalg import inv
from tqdm import tqdm

from .utils import *


def log_b(x, b=10):
    return np.log(x) / np.log(b)


def sq_deltat(X, delta_t, q):
    TT = len(X)
    N = int(np.ceil(TT / delta_t))
    ans = 0.0
    for i in range(N):
        if (i + 1) * delta_t < TT:
            tmp = np.abs(X[(i + 1) * delta_t] - X[i * delta_t]) ** q
            ans += tmp
    return ans


def get_tau_q(sqa, TT):
    sqa["T"] = TT
    results = smf.ols(
        "log_b(Sq, b = 10) ~ log_b(dt, b = 10) + log_b(T, b = 10) - 1", data=sqa
    ).fit()
    tau_q = results.params.values[0]
    return tau_q


def get_q0_single(
    Xt, max_deltat, lower=0, upper=5, precision=0.001, return_tau_q=False
):
    TT = len(Xt)
    all_q = np.arange(lower, upper, precision)
    len_q = len(all_q)
    mat = np.zeros([max_deltat - 1, len_q])
    for i in range(len_q):
        tmp = sq_deltat(Xt, 1, all_q[i])
        # tmp = 1e-10 if tmp == 0 else tmp
        for deltat in range(1, max_deltat):
            mat[deltat - 1, i] = sq_deltat(Xt, deltat, all_q[i]) / tmp
    X = log_b(np.vstack([np.arange(1, max_deltat), [TT] * (max_deltat - 1)]).T, 10)
    tau_q = inv(X.T.dot(X)).dot(X.T).dot(log_b(mat, 10))[0,]
    if return_tau_q:
        return tau_q, all_q, mat
    else:
        return all_q[np.abs(tau_q).argmin()]


def get_q0(Xt, max_deltat, precision=1e-14):
    p = 1
    currQ0 = get_q0_single(Xt, max_deltat, precision=p)
    while p > precision:
        lower = currQ0 - p
        lower = 0 if lower < 0 else lower
        currQ0_new = get_q0_single(
            Xt, max_deltat, lower=lower, upper=currQ0 + p, precision=p / 2
        )
        p = p / 2
        currQ0 = currQ0_new
    return currQ0


def get_falphas_single(Xt, max_deltat, lower=0, upper=2, precision=0.1, etol=1e-6):
    all_a = np.arange(lower, upper, precision)
    f_alphas = np.zeros([len(all_a)])
    p = 1
    tau_qs, qs, _ = get_q0_single(
        Xt, max_deltat, lower=0, upper=5, precision=p, return_tau_q=True
    )
    qmins = np.zeros([len(all_a)])
    for i in range(len(all_a)):
        qmins[i], f_alphas[i] = f_alpha(all_a[i], qs, tau_qs)
    choice = ((f_alphas - 1) ** 2).argmin()
    ans = all_a[choice]
    qmin = qmins[choice]
    qmin = qmin if qmin > 0 else 1
    err = 1
    while err > etol:
        tmpAns = ans.copy()
        tau_qs, qs, _ = get_q0_single(
            Xt,
            max_deltat,
            lower=qmin - p,
            upper=qmin + p,
            precision=p / 2,
            return_tau_q=True,
        )
        p = p / 2
        for i in range(len(all_a)):
            qmins[i], f_alphas[i] = f_alpha(all_a[i], qs, tau_qs)
        choice = ((f_alphas - 1) ** 2).argmin()
        ans = all_a[choice]
        qmin = qmins[choice]
        err = ans - tmpAns
    #         print(ans)
    if (upper - ans) < precision:
        ans = get_falphas(
            Xt, max_deltat, lower=lower, upper=upper + precision, precision=precision
        )
    return ans


def get_falphas(Xt, max_deltat, lower=0, upper=5, precision=0.1):
    p = 1
    ans = get_falphas_single(Xt, max_deltat, lower=lower, upper=upper, precision=p)
    while p > precision:
        lower = ans - p
        ans_new = get_falphas_single(
            Xt, max_deltat, lower=lower * (lower > 0), upper=ans + p, precision=p / 2
        )
        p = p / 2
        ans = ans_new
    return ans


def f_alpha(alpha, q, tau_q):
    fa = q * alpha - tau_q
    qmin = q[np.argmin(fa)]
    return qmin, min(fa)


class mmar_inference:
    def __init__(self, Pt):
        logging.info("Preparing data...")
        self.Xt = np.log(Pt) - np.log(Pt[0])
        self.TT = len(self.Xt)

    def fit(
        self,
        max_deltat=1000,
        min_box_size=0,
        precision=0.01,
        create_df=True,
    ):
        assert precision <= 0.1, "precision needs to be at least 0.1 ."
        Xt = self.Xt
        TT = self.TT
        Sq = {}
        if max_deltat > TT:
            max_deltat = TT - 1
        if min_box_size > 0:
            max_deltat = int(np.ceil(TT / min_box_size))
            logging.info("Fixing max_deltat to be {}".format(max_deltat))
        logging.info("Estimating Hurst parameter...")
        q0 = get_q0(Xt, max_deltat, precision=precision)
        H_ = 1.0 / q0
        logging.info(f"q0 = {q0:.4f}| H = {H_:.4f}")
        if H_ <= 0 or H_ > 2:
            raise Exception("The Hurst parameter must be in the interval (0,2]!")
        logging.info("Estimating alpha0")
        alpha0 = get_falphas(Xt, max_deltat, lower=0, upper=3, precision=precision)
        logging.info(f"alpha0 = {alpha0:.4f}")
        lambda_ = alpha0 / H_
        sigma2 = 2 * (np.abs(lambda_ - 1)) / log_b(2, b=10)
        logging.info(f"lambda = {lambda_:.4f}")
        logging.info(f"sigma2 = {sigma2:.4f}")

        self.min_box_size = min_box_size
        self.max_deltat = max_deltat
        self.H_ = H_
        self.alpha0 = alpha0
        self.lambda_ = lambda_
        self.sigma2 = sigma2
        if create_df:
            Sqm = get_q0_single(
                Xt, max_deltat, lower=0, upper=5, precision=1, return_tau_q=True
            )[2]
            Sq_df_m = pd.DataFrame(Sqm)
            Sq_df_m.columns = np.arange(0, 5)
            Sq_df_m.reset_index(inplace=True)
            Sq_df_m = Sq_df_m.melt(id_vars="index")
            Sq_df_m.columns = ["dt", "q", "Sq"]
            self.Sq_df_m = Sq_df_m

        return H_, alpha0, lambda_, sigma2

    def plot(self, qqs=None):
        if qqs is not None:
            df = self.Sq_df_m.query("q in {}".format(qqs))
        else:
            df = self.Sq_df_m
        df["l_dt"] = np.log(df["dt"])
        df["l_Sq"] = np.log(df["Sq"])
        df["q"] = df["q"].astype("float")

        sns.scatterplot(x="l_dt", y="l_Sq", hue="q", data=df)
        plt.show()
