import json
import logging
import multiprocessing
from datetime import datetime

import numba as nb
import numpy as np
import pandas as pd
from joblib import Parallel, delayed
from scipy.integrate import simpson as simps
from scipy.stats import norm
from tqdm import tqdm

from maiagomes.data import get_options_prices
from maiagomes.models.allocate import get_kelly_c
from maiagomes.models.mmar.montecarlo import MMAR_simulator_handler
from maiagomes.models.mmar.utils import ecdf_py, moving_average

logger = logging.getLogger(__name__)


def black_scholes_merton_price_minutes(S, K, T_minutes, r, sigma, option_type="call"):
    """
    Calculate the Black-Scholes-Merton option price with time to expiration in minutes.

    Parameters:
    S: Current stock price
    K: Option strike price
    T_minutes: Time to expiration (in minutes)
    r: Risk-free interest rate
    sigma: Volatility of the underlying stock
    option_type: 'call' for call option, 'put' for put option

    Returns:
    Option price
    """
    T_years = T_minutes / (365 * 24 * 60)  # Convert minutes to years
    d1 = (np.log(S / K) + (r + 0.5 * sigma**2) * T_years) / (sigma * np.sqrt(T_years))
    d2 = d1 - sigma * np.sqrt(T_years)

    if option_type == "call":
        option_price = S * norm.cdf(d1) - K * np.exp(-r * T_years) * norm.cdf(d2)
    elif option_type == "put":
        option_price = K * np.exp(-r * T_years) * norm.cdf(-d2) - S * norm.cdf(-d1)

    return option_price


class Option_pricing_simulator:
    def __init__(self, simulator: MMAR_simulator_handler):
        self.simulator = simulator

    def define(
        self,
        ticker_symbol: str,
        expiration_date: str,
        minutes_until_expiration: int = None,
    ):
        self.ticker_symbol = ticker_symbol
        self.expiration_date = expiration_date
        self.option_chain_df = get_options_prices(
            ticker_symbol=ticker_symbol, expiration_date=expiration_date
        ).set_index("contractSymbol")
        if minutes_until_expiration is None:
            days_until_expiration = (
                datetime.strptime(expiration_date, "%Y-%m-%d").date()
                - datetime.now().date()
            ).days
            minutes_until_expiration = (days_until_expiration - 1) * 60 * 24
        self.option_chain_df["minutes_until_expiration"] = minutes_until_expiration
        self.option_chain_df["strike_distance"] = np.sqrt(
            (self.option_chain_df["S"] - self.option_chain_df["strike"]) ** 2
        )
        self.option_chain_df["rank"] = self.option_chain_df.groupby(
            ["inTheMoney", "direction"]
        ).strike_distance.transform("rank")
        self.option_chain_df.sort_values(
            by=["inTheMoney", "strike_distance"], inplace=True
        )

    def get_k_distance_contract(self, k, inTheMoney=False):
        return self.option_chain_df.query(
            f"rank == {k} and inTheMoney == {inTheMoney}"
        ).index.tolist()

    def mmar_black_scholes_merton_paths(
        self,
        minutes_until_expiration: int = None,
        S: float = None,
        K: float = None,
        sigma: float = None,
        direction: str = "both",
        options=None,
        return_log_returns=True,
    ):
        if options is None:
            options = {
                "_single_": {
                    "minutes_until_expiration": minutes_until_expiration,
                    "S": S,
                    "K": K,
                    "sigma": sigma,
                    "direction": direction,
                }
            }
        option_mat = np.zeros(self.simulator.mat.shape)
        for contract_name, option in options.items():
            option_mat += self.simulator.get_option_price_paths(**option)
        if return_log_returns:
            option_mat = np.nan_to_num(np.log(option_mat) - np.log(option_mat[0, 0]))
        return option_mat

    def get_survival_probabilities(
        self,
        mat,
        min_thr=390,
        call_mode=True,
        Teff=1.0,
        precision=0.5,
        tol=1e-5,
        numba_way=False,
        use_moving_average=True,
        window_size=30,
    ):
        if use_moving_average:
            mat = moving_average(mat, window_size=window_size, axis=0)
        returns = np.arange(precision, 1000, precision)
        if numba_way:
            probs = get_survival_probability(
                np.sort(mat, axis=0),
                np.log(returns),
                min_thr,
                tol=tol,
            )
            size = probs.size
            probs = dict(zip(returns[:size], probs))
            kelly_cs = {k: get_kelly_c(p=v, b=k - 1) for k, v in probs.items()}
            return probs, kelly_cs

        def get_survival(mat, thr=(min_thr / mat.shape[0])):
            return ((1 - mat) > thr).mean()

        ecdf_bypath = ecdf_py(mat, axis=0, aggregate_function=get_survival)
        probs = {}
        kelly_cs = {}
        for x in returns:
            survival_prob = ecdf_bypath(np.log(x))
            probs[x] = survival_prob

            kelly_cs[x] = get_kelly_c(p=survival_prob, b=x - 1)
            if survival_prob < tol:
                return probs, kelly_cs

        return probs, kelly_cs

    def assess_option_strategy(
        self,
        contracts: list,
        min_thr=390,
        precision=0.25,
        tol=1e-5,
        numba_way=True,
        **kwargs,
    ):
        options, cost = self.prep_options_to_simulate_path(contracts)
        _direction_ = [v["direction"] for v in options.values()]
        _direction_.sort()
        _type_ = "+".join(set(_direction_))
        mat = self.mmar_black_scholes_merton_paths(options=options)

        probs, kelly_cs = self.get_survival_probabilities(
            mat,
            min_thr=min_thr,
            precision=precision,
            tol=tol,
            numba_way=numba_way,
            **kwargs,
        )

        probs_mat = np.array(list(probs.values()))
        kelly_c_mat = np.array(list(kelly_cs.values()))
        returns_mat = np.array(list(probs.keys()))
        expected_return = simps(probs_mat, x=returns_mat)
        kelly_c_area = simps(kelly_c_mat, x=returns_mat)
        second_moment = 2 * simps(returns_mat * probs_mat, x=returns_mat)
        variance = second_moment - expected_return**2
        p = self.simulator.get_path_probability(
            mat=mat,
            log_return=np.log(expected_return),
            min_thr=min_thr,
            call_mode=True,
            Teff=1.0,
        )
        kelly_c = get_kelly_c(p=p, b=expected_return - 1)
        closer_to_50ret = returns_mat[abs(returns_mat - 1.5).argmin()]
        closer_to_100ret = returns_mat[abs(returns_mat - 2).argmin()]
        kelly_c_50 = get_kelly_c(p=probs[closer_to_50ret], b=closer_to_50ret - 1)
        kelly_c_100 = get_kelly_c(p=probs[closer_to_100ret], b=closer_to_100ret - 1)
        exposure = (1 - p) * cost
        return {
            "expected_return": expected_return,
            "expiration_date": self.expiration_date,
            "variance": variance,
            "expected_lower_bound": np.exp(
                norm.ppf(0.05, np.log(expected_return), np.log(np.sqrt(variance)))
            ),
            "expected_upper_bound": np.exp(
                norm.ppf(0.95, np.log(expected_return), np.log(np.sqrt(variance)))
            ),
            "probability": p,
            "kelly_c": kelly_c,
            "prob_50": probs[closer_to_50ret],
            "kelly_c_50": kelly_c_50,
            "prob_100": probs[closer_to_100ret],
            "kelly_c_100": kelly_c_100,
            "exposure": exposure,
            "kelly_c_area": kelly_c_area,
            "kelly_divergency": expected_return - kelly_c_area,
            "cost": cost,
            "lastTradeDate": min([v["lastTradeDate"] for v in options.values()]),
            "type": _type_,
        }

    def assess_contracts(self, contracts, min_thr=390, **kwargs):
        ans = {
            "+".join(contracts): self.assess_option_strategy(
                contracts, min_thr=min_thr, **kwargs
            )
        }

        for c in contracts:
            ans[c] = self.assess_option_strategy([c], min_thr=min_thr, **kwargs)
        return ans

    def assess_all_contracts(
        self,
        showProgress=False,
        rank_max=None,
        try_inTheMoney=True,
        min_thr=390,
        parallel=False,
        **kwargs,
    ):
        if rank_max is None:
            rank_max = int(self.option_chain_df["rank"].max())
        if parallel:
            return self.assess_all_contracts_parallel(
                rank_max=rank_max,
                try_inTheMoney=try_inTheMoney,
                min_thr=min_thr,
                **kwargs,
            )

        ans = {}
        for k in tqdm(range(rank_max), disable=not showProgress):
            ans.update(
                self.assess_contracts(
                    self.get_k_distance_contract(k + 1), min_thr=min_thr, **kwargs
                )
            )
            if try_inTheMoney:
                try:
                    ans.update(
                        self.assess_contracts(
                            self.get_k_distance_contract(k + 1),
                            inTheMoney=True,
                            min_thr=min_thr,
                            **kwargs,
                        )
                    )
                except Exception as e:
                    logger.error("Error assessing inTheMoney contract")
                    continue
        return ans

    def assess_all_contracts_parallel(
        self, rank_max=None, try_inTheMoney=False, min_thr=390, **kwargs
    ):
        n_cores = multiprocessing.cpu_count() - 1
        _output_ = Parallel(n_jobs=n_cores)(
            delayed(self.assess_contracts)(
                self.get_k_distance_contract(k + 1), min_thr=min_thr, **kwargs
            )
            for k in range(rank_max)
        )
        if try_inTheMoney:
            _output_ += Parallel(n_jobs=n_cores)(
                delayed(self.assess_contracts)(
                    self.get_k_distance_contract(k + 1),
                    inTheMoney=True,
                    min_thr=min_thr,
                    **kwargs,
                )
                for k in range(rank_max)
            )

        ans = {}
        for _o_ in _output_:
            ans.update(_o_)
        return ans

    def prep_options_to_simulate_path(
        self, contracts: list, option_chain_df: pd.DataFrame = None
    ):
        if option_chain_df is None:
            option_chain_df = self.option_chain_df
        _option_chain_ = option_chain_df.loc[contracts].reset_index()
        cost = _option_chain_.lastPrice.sum()
        cols = [
            "contractSymbol",
            "lastTradeDate",
            "strike",
            "impliedVolatility",
            "direction",
            "S",
        ]
        if "minutes_until_expiration" in _option_chain_.columns:
            cols += ["minutes_until_expiration"]
        options_dict = (
            _option_chain_[cols]
            .rename(columns={"strike": "K", "impliedVolatility": "sigma"})
            .set_index("contractSymbol")
            .to_dict(orient="index")
        )
        return options_dict, cost


@nb.njit
def get_survival_probability(mat, return_arr, min_n_minutes, tol=1e-6):
    n_steps, n_paths = mat.shape
    probs = np.zeros(len(return_arr))
    kelly_c = np.zeros(len(return_arr))

    for i, _return_ in enumerate(return_arr):
        prob = 0
        for path in range(n_paths):
            if mat[-min_n_minutes, path] < _return_:
                continue
            for step in range(n_steps):
                if mat[step, path] > _return_:
                    count = n_steps - step
                    if count > min_n_minutes:
                        prob += 1
                    break
        probs[i] = prob / n_paths
        if probs[i] < tol:
            return probs[: i + 1]

    return probs
