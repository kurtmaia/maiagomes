import logging
import warnings

from statsmodels.tools.sm_exceptions import ConvergenceWarning

warnings.simplefilter("ignore", ConvergenceWarning)


import numpy as np
from scipy.stats import multinomial

logger = logging.getLogger(__name__)


def do_nothing(__input__):
    return __input__


class Allocate_w(object):
    def __init__(self, strategies):
        self.strategies = strategies

    def get(self, strategy, *args, **kwargs):
        return self.strategies[strategy](*args, **kwargs)
        return self.strategies[strategy](*args, **kwargs)


def get_random_portfolio(historyPrices, currDate):
    n_basket = historyPrices.shape[1]
    p = np.array([1.0 / n_basket] * n_basket)
    return (multinomial.rvs(n=n_basket, p=p) / n_basket).reshape([1, -1])


def get_simple_portfolio(historyPrices, currDate):
    n_basket = historyPrices.shape[1]
    return np.array([1.0 / n_basket] * n_basket).reshape([1, -1])


def get_kelly_c(p, a=1.0, b=1.0):
    if b < 0:
        _a_ = np.max([a, abs(b)])
        _b_ = 1
    else:
        _a_ = a
        _b_ = b
    return p / _a_ - (1 - p) / _b_


def multivariate_kelly_allocation(
    univariate_kelly_allocation, theta=None, memberships=None, node_idx=None
):
    if theta is None:
        theta = np.eye(len(univariate_kelly_allocation))
    if memberships is None:
        memberships = np.eye(len(univariate_kelly_allocation))
    _rho_ = memberships.dot(theta).dot(memberships.T)
    final_allocation = np.zeros(univariate_kelly_allocation.shape)
    if node_idx is None:
        node_idx = univariate_kelly_allocation.argmax()
    dish_idx = memberships[node_idx,].argmax()
    alloc = univariate_kelly_allocation[node_idx]
    final_allocation[node_idx] = alloc
    while alloc > 0.01 and alloc <= 1:
        theta[:, dish_idx] = 1.0
        _rho_ = memberships.dot(theta).dot(memberships.T)
        univariate_kelly_allocation = ((1 - alloc) * univariate_kelly_allocation) * (
            1 - _rho_[node_idx,]
        )
        node_idx = univariate_kelly_allocation.argmax()
        dish_idx = memberships[node_idx,].argmax()
        alloc = univariate_kelly_allocation[node_idx]
        final_allocation[node_idx] = alloc

    return final_allocation


def gen_design_matrix(arr, cats=None, **kwargs):
    nrow = len(arr)
    if cats is None:
        cats = np.unique(arr)
    ncol = len(cats)
    cats_dict = dict(zip(cats, range(ncol)))
    M = np.zeros([nrow, ncol], **kwargs)
    for i in range(nrow):
        M[i, cats_dict[arr[i]]] = 1.0
    return M
