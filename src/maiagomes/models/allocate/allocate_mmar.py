import json
import logging
from dataclasses import dataclass
from datetime import datetime
from typing import List

import numpy as np
import pandas as pd
from scipy.stats import dirichlet

from .utils import Allocate_w, do_nothing, get_simple_portfolio

logger = logging.getLogger(__name__)

@dataclass
class Allocate_mmar:
    """Estimate mmar allocation"""

    def __init__(
        self,
        mmar_results_file: str = None,
        mmar_results: dict = None,
        remap: dict = None,
        whitelisted_symbols: List = None,
        blacklisted_symbols: List = None,
    ):
        if mmar_results_file is not None:
            mmar_results = json.load(open(mmar_results_file, "rb"))
        if remap is not None:
            for k, v in remap.items():
                mmar_results[v] = mmar_results[k]
                del mmar_results[k]
        if whitelisted_symbols is not None:
            mmar_results = {
                k: v for k, v in mmar_results.items() if k in whitelisted_symbols
            }
        elif blacklisted_symbols is not None:
            mmar_results = {
                k: v for k, v in mmar_results.items() if k not in blacklisted_symbols
            }
        self.mmar_results = pd.DataFrame(mmar_results).T

    def simple(
        self,
        mmar_results,
        buying_power=1,
        full_output=False,
        wrapper_function=do_nothing,
    ):
        df = mmar_results.copy()
        df["norm_mu"] = df["mu"] / np.sqrt(df["sigma2"] / df["N"])
        p = df["norm_mu"].values
        p /= p.sum()
        df["alloc"] = wrapper_function(p * buying_power)
        if full_output:
            return df
        else:
            return df.alloc.to_dict()

    def allocate(self, strategies: dict = None, **kwargs):
        if strategies is None:
            strategies = {"mmar_alloc": self.simple}
        allocator = Allocate_w(strategies)
        return allocator.get("mmar_alloc", mmar_results=self.mmar_results, **kwargs)


def get_mmar_portfolio(
    historyPrices,
    currDate,
    mmar_results=None,
    min_date=datetime(2021, 11, 2, 0, 0),
    symbolList=None,
):
    latest_results_date = currDate.astype("datetime64[D]")
    results_do_not_exist = str(latest_results_date) not in mmar_results.keys()
    while results_do_not_exist:
        latest_results_date -= 1
        if datetime.strptime(str(latest_results_date), "%Y-%m-%d") < min_date:
            return get_simple_portfolio(historyPrices, currDate)
        else:
            results_do_not_exist = str(latest_results_date) not in mmar_results.keys()
    currEstimates = mmar_results[str(latest_results_date)]
    df = pd.DataFrame(currEstimates).T
    df["norm_mu"] = df["mu"] / np.sqrt(df["sigma2"] / df["N"])
    p = dirichlet.rvs(df.norm_mu.values)[0]
    df["alloc"] = p
    allocs = df.alloc.to_dict()
    ans = np.zeros(len(symbolList))
    for i, sym in enumerate(symbolList):
        if sym in allocs.keys():
            ans[i] = allocs[sym]
    ans /= ans.sum()
    return ans

