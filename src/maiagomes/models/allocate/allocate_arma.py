import logging
import warnings
from itertools import product
from typing import List

from statsmodels.tools.sm_exceptions import ConvergenceWarning

warnings.simplefilter("ignore", ConvergenceWarning)


import numpy as np
import pandas as pd
import statsmodels.api as sm
from scipy.stats import norm

from maiagomes.testing import Data_handler

from .utils import (Allocate_w, get_kelly_c, get_simple_portfolio,
                    multivariate_kelly_allocation)

logger = logging.getLogger(__name__)



class Allocate_arma:
    """Estimate arma allocation"""

    def __init__(
        self,
        symbols: list = None,
        time_series_df_path: str = None,
        time_series_df: pd.DataFrame = None,
        remap: dict = None,
        whitelisted_symbols: List = [],
        blacklisted_symbols: List = [],
        add_cash: bool = False,
        filter_out: str = "Date > '2023-01-01'",
    ):
        if symbols is None:
            time_series_df = pd.read_csv(time_series_df_path)
            if remap is not None:
                time_series_df = time_series_df.rename(columns=remap)
            self.data_h = Data_handler(
                df=time_series_df,
                date_var="Date",
                add_cash=add_cash,
                filter_out=filter_out,
            )

            if len(whitelisted_symbols) == 0:
                self.symbols = list(
                    set(self.data_h.symbolList) - set(blacklisted_symbols)
                )
            else:
                self.symbols = whitelisted_symbols
        else:
            self.symbols = symbols

    def simple(self, daily_df: pd.DataFrame = None, **kwargs):
        symbols = self.symbols

        if daily_df is None:
            daily_df = self.data_h.get_last_events_by("D").reset_index(drop=True)
        else:
            symbols = daily_df.columns
        expected_returns = np.zeros(len(symbols))
        for i, symb in enumerate(symbols):
            expected_returns[i] = get_arma_win_probability(
                np.log(daily_df[symb].values), **kwargs
            )[0]
        _expected_returns_ = np.nan_to_num(expected_returns, nan=0)
        _expected_returns_ = _expected_returns_ * (_expected_returns_ > 0)
        lnmat = np.log(daily_df.iloc[1:,]).diff(axis=0)[1:]
        lnmat_discretized = -1 * (lnmat < 0) + (lnmat > 0)
        weighted_graph = lnmat_discretized.T.dot(lnmat_discretized)
        weighted_graph_sum = (1.0 * (lnmat_discretized != 0)).T.dot(
            1.0 * (lnmat_discretized != 0)
        )
        _rho_ = np.nan_to_num(abs(weighted_graph / weighted_graph_sum))
        # _rho_ = np.nan_to_num(abs(np.corrcoef(lnmat.T)))
        _expected_returns_ = multivariate_kelly_allocation(
            _expected_returns_, theta=_rho_
        )
        if _expected_returns_.sum() > 1:
            logger.warning(
                f"Allocation suggests leveraging: {_expected_returns_.sum()} > 1 "
            )
            _expected_returns_ /= _expected_returns_.sum()
        elif _expected_returns_.sum() < 1:
            logger.warning(
                f"Allocation suggests saving {1- _expected_returns_.sum():.3%} in cash."
            )
        ans = dict(zip(symbols, list(_expected_returns_)))
        ans["_CASH_"] = 1 - _expected_returns_.sum()
        return ans

    def allocate(self, strategies: dict = None, **kwargs):
        if strategies is None:
            strategies = {"arma": self.simple}
        allocator = Allocate_w(strategies)
        return allocator.get("arma", **kwargs)




def get_arma_prediction(ts, p=-1, d=1, q=0, steps=1, **kwargs):
    if p == -1 or q == -1:
        order = auto_tune_arima(ts, **kwargs)
    else:
        order = (p, d, q)
    model = sm.tsa.ARIMA(ts, order=order)
    results = model.fit()

    forecast_values = results.get_forecast(steps=steps)
    return forecast_values

def get_arma_expected_return(ts, stdize=False, **kwargs):
    preds = get_arma_prediction(ts, **kwargs)
    ans = np.log(preds.predicted_mean) - np.log(ts[-1])
    if stdize:
        ans += -np.log(preds.se_mean)
    return ans

def get_arma_win_probability(ts, **kwargs):
    preds = get_arma_prediction(ts, **kwargs)
    diff = preds.predicted_mean - ts[-1]
    p = norm.cdf(diff / preds.se_mean)
    return get_kelly_c(p, b=np.exp(diff)[0])

def get_arma_portfolio(historyPrices, currDate, past_size=10, p=1, d=1, q=-1, **kwargs):
    if historyPrices.shape[0] <= past_size:
        return get_simple_portfolio(historyPrices, currDate)
    expected_returns = np.zeros(historyPrices.shape[1])
    for i in range(historyPrices.shape[1] - 1):
        expected_returns[i] = get_arma_win_probability(
            np.log(historyPrices[:, i]), p=p, d=d, q=q, **kwargs
        )[0]
    _expected_returns_ = np.nan_to_num(expected_returns, nan=0)
    _expected_returns_ = _expected_returns_ * (_expected_returns_ > 0)
    lnmat = np.diff(np.log(historyPrices), axis=0)
    lnmat_discretized = -1 * (lnmat < 0) + (lnmat > 0)
    weighted_graph = lnmat_discretized.T.dot(lnmat_discretized)
    weighted_graph_sum = (1.0 * (lnmat_discretized != 0)).T.dot(
        1.0 * (lnmat_discretized != 0)
    )
    _rho_ = np.nan_to_num(abs(weighted_graph / weighted_graph_sum))
    # _rho_ = np.nan_to_num(abs(np.corrcoef(lnmat.T)))
    _expected_returns_ = multivariate_kelly_allocation(_expected_returns_, _rho_)
    if _expected_returns_.sum() > 1:
        logger.warning(
            f"Allocation suggests leveraging: {_expected_returns_.sum()} > 1 "
        )
        _expected_returns_ /= _expected_returns_.sum()
    elif _expected_returns_.sum() < 1:
        logger.warning(
            f"Allocation suggests saving {1- _expected_returns_.sum():.3%} in cash."
        )
        _expected_returns_[-1] = 1 - _expected_returns_.sum()
    return _expected_returns_.reshape([1, -1])



def auto_tune_arima(
    ts, p_values=range(0, 3), d_values=range(0, 2), q_values=range(0, 3)
):
    split_point = int(len(ts) * 0.9)  # Adjust the split percentage as needed

    # Split the time series into training and testing sets
    train_data = ts[:split_point]
    test_data = ts[split_point:]
    best_mse = float("inf")
    best_order = None

    for p, d, q in product(p_values, d_values, q_values):
        model = sm.tsa.ARIMA(train_data, order=(p, d, q))
        try:
            results = model.fit()
            predicted = results.get_forecast(steps=len(test_data))
            error = predicted.predicted_mean - test_data
            mse = (error**2).sum()
            # aic = results.aic
            if mse < best_mse:
                best_mse = mse
                best_order = (p, d, q)
        except:
            continue
    return best_order
    return best_order
    return best_order
    return best_order
