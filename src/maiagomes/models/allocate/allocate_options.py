import logging
import warnings
from datetime import datetime, timedelta
from itertools import product
from typing import List, Union

import pytz
from statsmodels.tools.sm_exceptions import ConvergenceWarning

warnings.simplefilter("ignore", ConvergenceWarning)


import numpy as np
import pandas as pd
import statsmodels.api as sm
from scipy.stats import norm

from maiagomes.models.allocate.utils import (
    Allocate_w,
    gen_design_matrix,
    multivariate_kelly_allocation,
)

logger = logging.getLogger(__name__)


class Allocate_options:
    """Estimate options allocation"""

    def __init__(
        self,
        options_assessment_df: Union[pd.DataFrame, str],
        latest_values: pd.DataFrame,
        min_cost: float = 0.0,
        max_cost: float = np.Inf,
        univariate_allocation_variable="kelly_c",
        min_last_trade_time_minutes=6 * 60,
    ):
        self.univariate_allocation_variable = univariate_allocation_variable
        if isinstance(options_assessment_df, str):
            options_assessment_df = (
                pd.read_csv(options_assessment_df)
                .rename(columns={"Unnamed: 0": "Symbol"})
                .dropna(subset="contractSymbol")
            )
        self.assessment_df = options_assessment_df.query(
            f"expected_return > 1 and {univariate_allocation_variable} > 0 and cost >= @min_cost and cost <= @max_cost"
        ).reset_index(drop=True)

        if min_last_trade_time_minutes is not None:
            self.assessment_df["lastTradeDate"] = pd.to_datetime(
                self.assessment_df["lastTradeDate"]
            )
            self.assessment_df["lastTradeDate"] = self.assessment_df[
                "lastTradeDate"
            ].dt.tz_convert("America/Chicago")

            self.assessment_df["flagged_in"] = (
                datetime.now(pytz.timezone("America/Chicago"))
                - self.assessment_df["lastTradeDate"]
            ) < timedelta(minutes=min_last_trade_time_minutes)
            self.assessment_df = self.assessment_df.query("flagged_in").reset_index(
                drop=True
            )

        self.latest_values = latest_values

    def simple(self, latest_values=None, node_idx=None):
        if latest_values is None:
            latest_values = self.latest_values
        assert latest_values is not None, "latest_values is None"
        symbols = list(set(self.assessment_df.Symbol))
        bernoulli_graph = abs(
            self.estimateBernoulliGraph(latest_values[symbols]).fillna(0)
        )
        allocations = self.assessment_df[self.univariate_allocation_variable].values
        if node_idx is None:
            if (allocations == 1).sum() > 1:
                logger.info(
                    f"Maximal allocation of {self.univariate_allocation_variable} is 1"
                )
                node_idx = self.assessment_df.sort_values(
                    by=[self.univariate_allocation_variable, "cost"],
                    ascending=[False, True],
                ).index[0]
            else:
                node_idx = None
        _recommended_allocation_ = multivariate_kelly_allocation(
            allocations,
            theta=bernoulli_graph.values,
            memberships=gen_design_matrix(
                self.assessment_df.Symbol.tolist(), cats=bernoulli_graph.index.tolist()
            ),
            node_idx=node_idx,
        )
        if _recommended_allocation_.sum() > 1:
            logger.warning(
                f"Allocation suggests leveraging: {_recommended_allocation_.sum()} > 1"
            )
            _recommended_allocation_ /= _recommended_allocation_.sum()
        elif _recommended_allocation_.sum() < 1:
            logger.warning(
                f"Allocation suggests saving {1- _recommended_allocation_.sum():.3%} in cash."
            )
        ans = dict(
            zip(list(self.assessment_df.contractSymbol), list(_recommended_allocation_))
        )
        ans["_CASH_"] = 1 - _recommended_allocation_.sum()
        return ans

    def allocate(self, strategies: dict = None, **kwargs):
        if strategies is None:
            strategies = {"options": self.simple}
        allocator = Allocate_w(strategies)
        return allocator.get("options", **kwargs)

    def estimateBernoulliGraph(self, latest_values):
        lnmat = np.log(latest_values.iloc[1:,]).diff(axis=0)[1:]
        lnmat_discretized = -1 * (lnmat < 0) + (lnmat > 0)
        weighted_graph = lnmat_discretized.T.dot(lnmat_discretized)
        weighted_graph_sum = (1.0 * (lnmat_discretized != 0)).T.dot(
            1.0 * (lnmat_discretized != 0)
        )
        return weighted_graph / weighted_graph_sum
