import datetime
import os
from dataclasses import dataclass
from typing import Callable, Dict, Iterator, List, Optional, Union

import numpy as np
import pandas as pd
from alpaca.data.historical import CryptoHistoricalDataClient, StockHistoricalDataClient
from alpaca.data.requests import (
    CryptoBarsRequest,
    CryptoLatestQuoteRequest,
    StockBarsRequest,
    StockLatestQuoteRequest,
)
from alpaca.data.timeframe import TimeFrame


@dataclass
class Pull_bars_from_alpaca:
    stock_client: StockHistoricalDataClient
    crypto_client: CryptoHistoricalDataClient
    start: datetime = None
    end: datetime = None
    timeframe: Optional[TimeFrame] = TimeFrame.Day

    def pull_crypto(self, symbols: list):
        _older_ = pd.DataFrame()
        if self.start is not None:
            request_params = CryptoBarsRequest(
                symbol_or_symbols=symbols,
                timeframe=self.timeframe,
                start=self.start,
                end=datetime.datetime.now().date() if self.end is None else self.end,
            )
            bars = self.crypto_client.get_crypto_bars(request_params)
            _older_ = bars.df.reset_index()

        request_params = CryptoLatestQuoteRequest(symbol_or_symbols=symbols)
        request_result = self.crypto_client.get_crypto_latest_bar(request_params)
        _latest_ = pd.concat(
            [pd.DataFrame(v).set_index(0).T for v in request_result.values()]
        )
        if _older_.empty:
            ans = _latest_
        else:
            ans = pd.concat([_older_, _latest_])

        ans = (
            ans.pivot(index="timestamp", columns="symbol", values="close")
            .sort_index()
            .reset_index()
            .infer_objects(copy=False)
            .ffill()
        )

        return ans

    def pull_stock(self, symbols: list):
        _older_ = pd.DataFrame()
        if self.start is not None:
            request_params = StockBarsRequest(
                symbol_or_symbols=symbols,
                timeframe=self.timeframe,
                start=self.start,
                end=datetime.datetime.now().date() if self.end is None else self.end,
            )
            bars = self.stock_client.get_stock_bars(request_params)
            _older_ = bars.df.reset_index()

        request_params = StockLatestQuoteRequest(symbol_or_symbols=symbols)
        request_result = self.stock_client.get_stock_latest_bar(request_params)
        _latest_ = pd.concat(
            [pd.DataFrame(v).set_index(0).T for v in request_result.values()]
        )
        if _older_.empty:
            ans = _latest_
        else:
            ans = pd.concat([_older_, _latest_])
        ans = (
            ans.drop_duplicates(subset=["symbol", "timestamp"])
            .pivot(index="timestamp", columns="symbol", values="close")
            .sort_index()
            .reset_index()
            .infer_objects(copy=False)
            .ffill()
        )

        return ans

    def pull(
        self,
        symbols: list,
        crypto: Optional[List] = [],
        stocks: Optional[List] = [],
        known_crypto_symbols: Optional[List] = [
            "BTC",
            "ETH",
            "BNB",
            "SOL",
            "XRP",
            "ADA",
            "LUNA",
        ],
    ):
        remap_dict = {}
        for sym in symbols:
            if sym in known_crypto_symbols:
                new_sym = f"{sym}/USD"
                remap_dict[new_sym] = sym
                crypto.append(new_sym)
            else:
                stocks.append(sym)
        if len(crypto) > 0:
            crypto_df = self.pull_crypto(symbols=crypto)
        else:
            crypto_df = pd.DataFrame()
        if len(stocks) > 0:
            stocks_df = self.pull_stock(symbols=stocks)
        else:
            stocks_df = pd.DataFrame()

        if crypto_df.empty:
            return stocks_df
        elif stocks_df.empty:
            return crypto_df
        else:
            ans = (
                crypto_df.merge(stocks_df, on="timestamp", how="outer")
                .infer_objects(copy=False)
                .ffill()
            )

            return ans


@dataclass
class Backfill_minute_bars:
    symbol: str
    symbol_type: str = "stock"
    stock_client: StockHistoricalDataClient = None
    crypto_client: CryptoHistoricalDataClient = CryptoHistoricalDataClient()
    start: datetime = datetime.datetime(2021, 2, 5)
    end: datetime = None
    timeframe: Optional[TimeFrame] = TimeFrame.Minute
    cache_folder: str = None

    def __post_init__(self):
        if self.stock_client is None:
            self.stock_client = StockHistoricalDataClient(
                os.getenv("APCA_API_KEY_ID"), os.getenv("APCA_API_SECRET_KEY")
            )
        self.filename = os.path.join(
            self.cache_folder, self.symbol_type, f"{self.symbol}.csv"
        )
        if os.path.exists(self.filename):
            self.df = pd.read_csv(self.filename)
            self.start = datetime.datetime.fromisoformat(
                self.df.iloc[-1]["timestamp"]
            ).date() - datetime.timedelta(days=1)
        else:
            self.df = None

    def get_quotes(self):
        if self.symbol_type == "stock":
            request_params = StockBarsRequest(
                symbol_or_symbols=self.symbol,
                timeframe=TimeFrame.Minute,
                start=self.start,
                end=datetime.datetime.now().date(),
            )

            bars = self.stock_client.get_stock_bars(request_params)

        elif self.symbol_type == "crypto":
            request_params = CryptoBarsRequest(
                symbol_or_symbols=f"{self.symbol}/USD",
                timeframe=TimeFrame.Minute,
                start=self.start,
                end=datetime.datetime.now().date(),
            )

            bars = self.crypto_client.get_crypto_bars(request_params)
        _df_ = bars.df.reset_index()
        if self.df is not None:
            _df_ = pd.concat([self.df, _df_])
            _df_["timestamp"] = pd.to_datetime(_df_["timestamp"], utc=True)
            _df_ = _df_.drop_duplicates(subset="timestamp", keep="last")
        return _df_.sort_values(by="timestamp")

    def update(self):
        self.get_quotes().to_csv(self.filename, index=False)
