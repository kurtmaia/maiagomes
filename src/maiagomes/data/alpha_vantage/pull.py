import os
from dataclasses import dataclass

from alpha_vantage.foreignexchange import ForeignExchange
from alpha_vantage.timeseries import TimeSeries


class get_alpha_vantage(object):
    def __init__(
        self,
        df=None,
        symbol=None,
        key=None,
        extended=False,
        min_partition=0,
        max_partition=5,
        interval="daily",
        outputsize="full",
    ):
        if key is None:
            try:
                key = os.getenv("ALPHA_VANTAGE_KEY")
            except Exception as err:
                print(err)
        ts = TimeSeries(key=key, output_format="pandas")
        fe = ForeignExchange(key=key, output_format="pandas")
        if df is None:
            if extended:
                df = get_intraday_extended(
                    symbol,
                    apikey=key,
                    interval=interval,
                    min_partition=min_partition,
                    max_partition=max_partition,
                )
            else:
                if interval == "daily":
                    df, meta_data = ts.get_daily(symbol=symbol, outputsize=outputsize)
                else:
                    df, meta_data = ts.get_intraday(
                        symbol=symbol, interval=interval, outputsize=outputsize
                    )
            self.meta_data = meta_data
            df = df.reset_index()
        df["date"] = pd.to_datetime(df["date"])
        df.columns = ["Date", "Open", "High", "Low", "Close", "Volume"]
        df = df.sort_values("Date").reset_index()
        df.drop("index", axis=1, inplace=True)
        self.df = df

    def filter(self, min_date=None, max_date=None):
        df = self.df
        if min_date is not None:
            df = df.loc[df.Date >= min_date]
        if max_date is not None:
            df = df.loc[df.Date < max_date]
        df["var"] = df.High.values - df.Low.values
        df = df.sort_values("Date").reset_index()
        df["diff"] = df.Close.diff()
        df["lag"] = df["Close"].shift(1)
        return df
