import pandas as pd
import yfinance as yf


def get_options_prices(ticker_symbol, expiration_date="2024-03-01"):
    # Fetch the option chain for the given symbol and expiration date
    option_chain = yf.Ticker(ticker_symbol).option_chain(expiration_date)
    y0 = yf.Ticker(ticker_symbol).history(period="1d")["Close"].iloc[-1]
    # Access call and put option data
    calls = option_chain.calls
    calls["direction"] = "call"
    puts = option_chain.puts
    puts["direction"] = "put"

    ans = pd.concat([puts, calls])
    ans["S"] = y0
    return ans
