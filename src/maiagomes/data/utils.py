from datetime import datetime, timedelta


def get_days_until_friday(given_date=datetime.now().date(), weeks=0):
    days_until_friday = (4 - given_date.weekday() + 7) % 7
    return days_until_friday + 7 * weeks


def closest_friday(given_date=datetime.now().date(), days_until_friday=None):
    # Calculate the difference in days between the given date and Friday (weekday 4)
    if days_until_friday is None:
        days_until_friday = (4 - given_date.weekday() + 7) % 7

    # Calculate the date of the closest Friday
    closest_friday_date = given_date + timedelta(days=days_until_friday)

    return closest_friday_date


def get_weekly_schedule():
    """ """
