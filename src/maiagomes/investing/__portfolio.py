import logging
import os
from dataclasses import dataclass
from datetime import datetime, timedelta
from typing import Callable, Dict, Iterator, List, Optional, Union

import numpy as np
import pandas as pd
import pytz
import requests
from alpaca.trading.client import TradingClient
from alpaca.trading.enums import OrderSide, TimeInForce
from alpaca.trading.requests import MarketOrderRequest

logger = logging.getLogger(__name__)


def get_portfolio_history(paper=True, ndays=60):
    if paper:
        domain = "https://paper-api.alpaca.markets"
        key = os.getenv("APCA_API_KEY_ID")
        secret = os.getenv("APCA_API_SECRET_KEY")
    else:
        domain = "https://api.alpaca.markets"
        key = os.getenv("APCA_LIVE_API_KEY_ID")
        secret = os.getenv("APCA_LIVE_API_SECRET_KEY")

    headers = {
        "accept": "application/json",
        "APCA-API-KEY-ID": key,
        "APCA-API-SECRET-KEY": secret,
    }

    response = requests.get(
        os.path.join(domain, f"v2/account/portfolio/history?period={ndays}D"),
        headers=headers,
    )
    ans_df = pd.DataFrame(response.json())
    ans_df["date"] = (
        pd.to_datetime(ans_df["timestamp"], unit="s")
        .dt.tz_localize("UTC")
        .dt.tz_convert("America/Chicago")
    )

    try:
        ### Get cash deposits and withdraws
        response = requests.get(
            os.path.join(domain, "v2/account/activities/CSD"), headers=headers
        )
        cash_df = pd.DataFrame(response.json())
        if not cash_df.empty:
            cash_df["date_only"] = pd.to_datetime(cash_df["date"])
            ans_df["date_only"] = pd.to_datetime(ans_df["date"].map(lambda a: a.date()))
            ans_df = ans_df.merge(
                cash_df[["date_only", "net_amount"]], on="date_only", how="outer"
            )
            ans_df.net_amount = ans_df.net_amount.fillna(0).astype(float)
            ans_df["cumulative_profit_loss"] = 100 * (
                ans_df["equity"] / ans_df["net_amount"].cumsum() - 1
            )
        ans_df["profit_loss"] = ans_df["equity"].diff().fillna(0)
        ans_df["profit_loss_pct"] = ans_df["profit_loss"] / ans_df["equity"]
    except Exception as err:
        logger.warning(err)

    return ans_df.sort_values("date").drop(columns=["timestamp"])


def check_sym(sym, TradingClient: TradingClient, change_if_crypto: bool = True):
    try:
        a = TradingClient.get_asset(sym)
        if sym == a.symbol:
            return a
    except Exception as e:
        logger.warning(e)
        if change_if_crypto:
            new_sym = f"{sym}/USD"
            try:
                a = TradingClient.get_asset(new_sym)
            except Exception as e:
                logger.warning(e)
                return None
            if new_sym == a.symbol:
                logger.info(f"Changing {sym} to {new_sym}")
                return a
        else:
            return None


@dataclass
class RebalancePortfolio:
    """Rebalance Portfolio based on weights"""

    TradingClient: TradingClient
    portfolio_weights: Optional[Dict] = None
    orders: Optional[Dict] = None

    def get_current_w(self):
        positions = self.TradingClient.get_all_positions()
        acc = self.TradingClient.get_account()
        total_tradable_money = float(acc.equity)
        curr_w = {pos.symbol: float(pos.market_value) for pos in positions}
        w = np.array(list(curr_w.values()))
        equity = w.sum()
        w /= total_tradable_money
        curr_w = dict(zip(curr_w.keys(), w))
        if w.sum() < 1:
            curr_w["_CASH_"] = 1 - w.sum()
        return curr_w, equity

    def validate_portfolio_weights(self, portfolio_weights: Optional[Dict]):
        if portfolio_weights is None:
            portfolio_weights = self.portfolio_weights
        assert portfolio_weights is not None, "portfolio_weights cannot be None."
        return portfolio_weights

    def check_symbols(self, portfolio_weights: Dict):
        alloc_w = {}
        crypto_assets = []
        rebalancing_positions = []
        closing_positions = []
        openning_positions = []
        current_w, equity = self.get_current_w()
        all_open_positions = list(current_w.keys())

        for k, v in portfolio_weights.items():
            metadata = check_sym(k, TradingClient=self.TradingClient)
            if metadata and metadata.tradable and metadata.fractionable:
                alloc_w[metadata.symbol] = v
                perc_change = np.round(v, 2)
                if metadata.symbol in all_open_positions:
                    if perc_change == 0:
                        closing_positions.append(metadata.symbol)
                    else:
                        rebalancing_positions.append(metadata.symbol)
                elif perc_change > 0:
                    openning_positions.append(metadata.symbol)

                if metadata.asset_class == "crypto":
                    crypto_assets += [metadata.symbol, metadata.symbol.replace("/", "")]

        closing_positions = list(
            set(closing_positions).union(
                set(all_open_positions) - set(portfolio_weights) - set(["_CASH_"])
            )
        )
        return (
            alloc_w,
            crypto_assets,
            rebalancing_positions,
            closing_positions,
            openning_positions,
        )

    def create_orders(
        self,
        alloc_w,
        crypto_assets,
        rebalancing_positions,
        closing_positions,
        openning_positions,
        total_tradable_money: float = None,
        percent_equity_to_trade: float = None,
    ):
        if total_tradable_money is None:
            acc = self.TradingClient.get_account()
            total_tradable_money = float(acc.equity)
        if percent_equity_to_trade is not None:
            total_tradable_money = percent_equity_to_trade * total_tradable_money

        orders = {"sell": {}, "buy": {}, "status": "pending"}

        for sym in closing_positions:
            params = {
                "symbol": sym,
                "qty": float(self.TradingClient.get_open_position(sym).qty),
                "side": OrderSide.SELL,
                "time_in_force": (
                    TimeInForce.IOC if sym in crypto_assets else TimeInForce.DAY
                ),
            }
            orders["sell"][sym] = MarketOrderRequest(**params)

        for sym in rebalancing_positions:
            rebalance_value = alloc_w[sym] * total_tradable_money - float(
                self.TradingClient.get_open_position(sym).market_value
            )
            if abs(rebalance_value) < 1:
                logger.info(f"Skipping {sym} since rebalance_value < 1.")
                continue
            isSell = rebalance_value < 0
            params = {
                "symbol": sym,
                "notional": np.round(abs(rebalance_value), 2),
                "side": OrderSide.SELL if isSell else OrderSide.BUY,
                "time_in_force": (
                    TimeInForce.IOC if sym in crypto_assets else TimeInForce.DAY
                ),
            }
            if isSell:
                orders["sell"][sym] = MarketOrderRequest(**params)
            else:
                orders["buy"][sym] = MarketOrderRequest(**params)

        for sym in openning_positions:
            params = {
                "symbol": sym,
                "notional": np.round(alloc_w[sym] * total_tradable_money, 2),
                "side": OrderSide.BUY,
                "time_in_force": (
                    TimeInForce.IOC if sym in crypto_assets else TimeInForce.DAY
                ),
            }
            orders["buy"][sym] = MarketOrderRequest(**params)

        return orders

    def prep_orders(
        self,
        portfolio_weights: Optional[Dict] = None,
        total_tradable_money: float = None,
        percent_equity_to_trade: float = None,
    ):
        portfolio_weights = self.validate_portfolio_weights(portfolio_weights)
        logger.info("Checking for mispelled symbols")
        (
            alloc_w,
            crypto_assets,
            rebalancing_positions,
            closing_positions,
            openning_positions,
        ) = self.check_symbols(portfolio_weights)
        self.orders = self.create_orders(
            alloc_w,
            crypto_assets,
            rebalancing_positions,
            closing_positions,
            openning_positions,
            total_tradable_money=total_tradable_money,
            percent_equity_to_trade=percent_equity_to_trade,
        )
        logger.info("Orders for portfolio rebalance preparation completed.")

    def submit_orders(self, orders):
        market_order = {}
        for k, v in orders["sell"].items():
            if k == "status":
                continue
            try:
                market_order[k] = self.TradingClient.submit_order(order_data=v)
            except Exception as e:
                logger.warning(e)
                continue
        logger.info("Sell orders submitted.")

        for k, v in orders["buy"].items():
            if k == "status":
                continue
            try:
                market_order[k] = self.TradingClient.submit_order(order_data=v)
            except Exception as e:
                logger.warning(e)
                continue
        logger.info("Buy orders submitted.")
        return market_order

    def run(self, wait_to_complete: bool = True):
        if self.orders is not None and self.orders["status"] == "pending":
            self.last_market_orders = self.submit_orders(self.orders)
            logger.info("Rebalance completed.")
            self.orders["status"] = "completed"
        else:
            logger.info("There are no orders to run.")
