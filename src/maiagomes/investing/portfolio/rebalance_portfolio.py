import logging
from dataclasses import dataclass
from typing import Dict, List, Optional

import numpy as np
from alpaca.trading.client import TradingClient
from alpaca.trading.enums import OrderSide, TimeInForce
from alpaca.trading.requests import MarketOrderRequest

logger = logging.getLogger(__name__)

from .utils import check_sym, get_current_w


@dataclass
class RebalancePortfolio:
    """Rebalance Portfolio based on weights"""

    TradingClient: TradingClient
    portfolio_weights: Optional[Dict] = None
    orders: Optional[Dict] = None

    def validate_portfolio_weights(self, portfolio_weights: Optional[Dict]):
        if portfolio_weights is None:
            portfolio_weights = self.portfolio_weights
        assert portfolio_weights is not None, "portfolio_weights cannot be None."
        return portfolio_weights

    def check_symbols(
        self, portfolio_weights: Dict, asset_classes: list = ["us_equity", "crypto"]
    ):
        alloc_w = {}
        crypto_assets = []
        rebalancing_positions = []
        closing_positions = []
        openning_positions = []
        current_w, equity = get_current_w(
            self.TradingClient, asset_classes=asset_classes
        )
        all_open_positions = list(current_w.keys())

        for k, v in portfolio_weights.items():
            metadata = check_sym(k, TradingClient=self.TradingClient)
            if metadata and metadata.tradable and metadata.fractionable:
                alloc_w[metadata.symbol] = v
                perc_change = np.round(v, 2)
                if metadata.symbol in all_open_positions:
                    if perc_change == 0:
                        closing_positions.append(metadata.symbol)
                    else:
                        rebalancing_positions.append(metadata.symbol)
                elif perc_change > 0:
                    openning_positions.append(metadata.symbol)

                if metadata.asset_class == "crypto":
                    crypto_assets += [metadata.symbol, metadata.symbol.replace("/", "")]

        closing_positions = list(
            set(closing_positions).union(
                set(all_open_positions) - set(portfolio_weights) - set(["_CASH_"])
            )
        )
        return (
            alloc_w,
            crypto_assets,
            rebalancing_positions,
            closing_positions,
            openning_positions,
        )

    def create_orders(
        self,
        alloc_w,
        crypto_assets,
        rebalancing_positions,
        closing_positions,
        openning_positions,
        total_tradable_money: float = None,
        percent_equity_to_trade: float = None,
    ):
        if total_tradable_money is None:
            acc = self.TradingClient.get_account()
            total_tradable_money = float(acc.equity)
        if percent_equity_to_trade is not None:
            total_tradable_money = percent_equity_to_trade * total_tradable_money

        orders = {"sell": {}, "buy": {}, "status": "pending"}

        for sym in closing_positions:
            params = {
                "symbol": sym,
                "qty": float(self.TradingClient.get_open_position(sym).qty),
                "side": OrderSide.SELL,
                "time_in_force": (
                    TimeInForce.IOC if sym in crypto_assets else TimeInForce.DAY
                ),
            }
            orders["sell"][sym] = MarketOrderRequest(**params)

        for sym in rebalancing_positions:
            rebalance_value = alloc_w[sym] * total_tradable_money - float(
                self.TradingClient.get_open_position(sym).market_value
            )
            if abs(rebalance_value) < 1:
                logger.info(f"Skipping {sym} since rebalance_value < 1.")
                continue
            isSell = rebalance_value < 0
            params = {
                "symbol": sym,
                "notional": np.round(abs(rebalance_value), 2),
                "side": OrderSide.SELL if isSell else OrderSide.BUY,
                "time_in_force": (
                    TimeInForce.IOC if sym in crypto_assets else TimeInForce.DAY
                ),
            }
            if isSell:
                orders["sell"][sym] = MarketOrderRequest(**params)
            else:
                orders["buy"][sym] = MarketOrderRequest(**params)

        for sym in openning_positions:
            params = {
                "symbol": sym,
                "notional": np.round(alloc_w[sym] * total_tradable_money, 2),
                "side": OrderSide.BUY,
                "time_in_force": (
                    TimeInForce.IOC if sym in crypto_assets else TimeInForce.DAY
                ),
            }
            orders["buy"][sym] = MarketOrderRequest(**params)

        return orders

    def prep_orders(
        self,
        portfolio_weights: Optional[Dict] = None,
        total_tradable_money: float = None,
        percent_equity_to_trade: float = None,
        asset_classes: list = ["us_equity", "crypto"],
    ):
        portfolio_weights = self.validate_portfolio_weights(portfolio_weights)
        logger.info("Checking for mispelled symbols")
        (
            alloc_w,
            crypto_assets,
            rebalancing_positions,
            closing_positions,
            openning_positions,
        ) = self.check_symbols(portfolio_weights, asset_classes=asset_classes)
        self.orders = self.create_orders(
            alloc_w,
            crypto_assets,
            rebalancing_positions,
            closing_positions,
            openning_positions,
            total_tradable_money=total_tradable_money,
            percent_equity_to_trade=percent_equity_to_trade,
        )
        logger.info("Orders for portfolio rebalance preparation completed.")

    def submit_orders(self, orders):
        market_order = {}
        for k, v in orders["sell"].items():
            if k == "status":
                continue
            try:
                market_order[k] = self.TradingClient.submit_order(order_data=v)
            except Exception as e:
                logger.warning(e)
                continue
        logger.info("Sell orders submitted.")

        for k, v in orders["buy"].items():
            if k == "status":
                continue
            try:
                market_order[k] = self.TradingClient.submit_order(order_data=v)
            except Exception as e:
                logger.warning(e)
                continue
        logger.info("Buy orders submitted.")
        return market_order

    def run(self, wait_to_complete: bool = True):
        if self.orders is not None and self.orders["status"] == "pending":
            self.last_market_orders = self.submit_orders(self.orders)
            logger.info("Rebalance completed.")
            self.orders["status"] = "completed"
        else:
            logger.info("There are no orders to run.")
