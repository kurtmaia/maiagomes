import logging
from dataclasses import dataclass
from datetime import datetime
from typing import Dict, List, Optional

import numpy as np
from alpaca.trading.client import TradingClient
from alpaca.trading.enums import OrderSide, TimeInForce
from alpaca.trading.requests import MarketOrderRequest
import re

logger = logging.getLogger(__name__)

from .utils import (
    get_all_positions,
    get_lastest_quote,
    get_position,
    get_trade_type,
    submit_order,
)


@dataclass
class OptionsTrader:
    """Rebalance Portfolio based on weights"""

    trading_client: TradingClient
    portfolio_weights: Optional[Dict] = None

    def trade(
        self,
        stop_loss: float = -0.2,
        take_profit: float = 0.5,
        total_tradable_money: float = None,
        percent_equity_to_trade: float = None,
        rebalance: bool = False,
        max_option_investment_rate: float = 0.5,
    ):
        # Start by liquidating options that have reached stop loss or take profit
        current_option_positions = self.get_current_option_positions()
        total_option_investment = sum(
            [float(pos["market_value"]) for pos in current_option_positions]
        )
        current_option_investment_rate = total_option_investment / float(
            self.trading_client.get_account().equity
        )
        if current_option_investment_rate > max_option_investment_rate:
            logger.info(
                "Option positions are greater than the maximum option investment rate. Reducing exposure by reducing stop loss to 5%."
            )
            stop_loss = -0.05
        liquidate_options = (
            self.select_options_to_liquidate_based_on_performance_and_date(
                current_option_positions, stop_loss, take_profit
            )
        )
        if len(liquidate_options) > 0:
            closing_oders = submit_order(
                self.trading_client, liquidate_options, wait_for_fill=True
            )
            logger.info(
                "Liquidated options that reached stop loss or take profit. We also liquidated options that less than 7 days to expiration."
            )
        else:
            closing_oders = []
        # Prepare buy orders

        if total_tradable_money is None:
            total_tradable_money = float(self.trading_client.get_account().cash)
            if rebalance:
                total_tradable_money += total_option_investment
        if percent_equity_to_trade is not None:
            total_tradable_money = percent_equity_to_trade * total_tradable_money
        logger.info(f"Total tradable money is {total_tradable_money}.")
        orders = self.prepare_trades(total_tradable_money)
        # Execute trades
        if len(orders) > 0:
            if len(orders["sell"]) > 0 and rebalance:
                logger.info("Closing positions.")
                closing_oders += submit_order(
                    self.trading_client, orders["sell"], wait_for_fill=True
                )
            if len(orders["buy"]) > 0:
                logger.info("Opening positions.")
                buy_orders = submit_order(
                    self.trading_client, orders["buy"], wait_for_fill=True
                )
            else:
                buy_orders = []
        else:
            buy_orders = []
        return closing_oders, buy_orders

    def prepare_trades(self, total_tradable_money: float):
        portfolio_weights = dict(
            sorted(
                self.portfolio_weights.items(), key=lambda item: item[1], reverse=True
            )
        )
        closing_positions, openning_positions = get_trade_type(
            self.trading_client, portfolio_weights, total_tradable_money
        )
        left_over_money_to_used_in_trading = total_tradable_money
        orders = {"sell": [], "buy": []}
        prices = get_lastest_quote(
            list(closing_positions.keys()) + list(openning_positions.keys()),
        )
        for symbol, selling_value in closing_positions.items():
            qty = min(
                np.ceil(selling_value / (100 * prices[symbol]["ap"])),
                float(get_position(self.trading_client, symbol)["qty"]),
            )
            closing_position = {
                "symbol": symbol,
                "qty": qty,
                "side": OrderSide.SELL,
                "time_in_force": TimeInForce.DAY,
            }
            left_over_money_to_used_in_trading += qty * prices[symbol]["ap"] * 100
            orders["sell"].append(MarketOrderRequest(**closing_position))
        for symbol, buying_value in openning_positions.items():
            if left_over_money_to_used_in_trading <= 0:
                continue
            notional = min(buying_value, 50000000)
            qty = min(np.floor(notional / (100 * prices[symbol]["ap"])), 1000)
            if qty > 0:
                open_position = {
                    "symbol": symbol,
                    "qty": qty,
                    "side": OrderSide.BUY,
                    "time_in_force": TimeInForce.DAY,
                }
                left_over_money_to_used_in_trading -= qty * prices[symbol]["ap"] * 100
                orders["buy"].append(MarketOrderRequest(**open_position))
        logger.info(
            f"Used {left_over_money_to_used_in_trading/total_tradable_money : .2%} of the initial budget.\n The remaining money is {left_over_money_to_used_in_trading}."
        )
        return orders

    def select_options_to_liquidate_based_on_performance_and_date(
        self,
        current_option_positions: list,
        stop_loss=-0.2,
        take_profit=0.5,
        days_to_expiration=7,
    ):
        liquidate_options = []
        for option in current_option_positions:
            if (
                float(option["unrealized_plpc"]) < stop_loss
                or float(option["unrealized_plpc"]) > take_profit
                or (option["expiration_date"] - datetime.now().date()).days
                <= days_to_expiration
            ):
                closing_order = {
                    "symbol": option["symbol"],
                    "qty": float(option["qty"]),
                    "side": OrderSide.SELL,
                    "time_in_force": TimeInForce.DAY,
                }
                liquidate_options.append(MarketOrderRequest(**closing_order))
        return liquidate_options

    def get_current_option_positions(self, asset_classes=["us_option"]):
        positions = get_all_positions(self.trading_client)

        ans = []
        for pos in positions:
            if pos["asset_class"] in asset_classes:
                pos["expiration_date"] = get_expiration_date_from_symbol(pos["symbol"])
                ans.append(pos)
        return ans


def find_p_or_c_position(option_symbol):
    """
    Finds the position of 'P' or 'C' between two numbers in an option symbol.

    Parameters:
    option_symbol (str): The option symbol (e.g., 'FCX250103P00039000').

    Returns:
    int: The position (index) of the detected 'P' or 'C', or -1 if not found.
    """
    match = re.search(r"(?<=\d)[PC](?=\d)", option_symbol)
    return match.start() if match else -1


def get_expiration_date_from_symbol(option_symbol):
    """
    Extracts the expiration date from an option symbol, accounting for variable ticker lengths.

    Parameters:
    option_symbol (str): The option symbol (e.g., 'FCX250103P00039000').

    Returns:
    str: The expiration date in YYYY-MM-DD format.
    """
    try:
        # Find the position of the expiration date
        # Ticker ends before the 6-digit YYMMDD part
        ticker_end = find_p_or_c_position(option_symbol)

        # Ensure valid ticker end position
        if ticker_end == -1:
            raise ValueError("Invalid option symbol format: missing 'P' or 'C'")

        # Extract the expiration date (YYMMDD)
        expiration_part = option_symbol[ticker_end - 6 : ticker_end]

        # Convert the YYMMDD string to a datetime object
        expiration_date = datetime.strptime(expiration_part, "%y%m%d").date()

        # Return the expiration date in YYYY-MM-DD format
        return expiration_date
    except Exception as e:
        return None
