import logging
import os
from time import sleep, time

import numpy as np
import pandas as pd
import requests
from alpaca.trading.client import TradingClient
from alpaca.trading.requests import MarketOrderRequest

logger = logging.getLogger(__name__)


def is_market_open(key, secret, paper=True):
    if paper:
        url = "https://paper-api.alpaca.markets/v2/clock"
    else:
        url = "https://api.alpaca.markets/v2/clock"
    headers = {
        "accept": "application/json",
        "APCA-API-KEY-ID": key,
        "APCA-API-SECRET-KEY": secret,
    }
    return requests.get(url, headers=headers).json()["is_open"]


def get_all_positions(trading_client):
    return trading_client.get("/positions")


def get_position(trading_client, symbol):
    return trading_client.get(f"/positions/{symbol}")


def get_trade_type(trading_client, portfolio_weights, total_tradable_money):
    current_positions = [pos["symbol"] for pos in get_all_positions(trading_client)]
    closing_positions = {}
    opening_positions = {}
    for symbol, weight in portfolio_weights.items():
        if symbol == "_CASH_":
            continue
        _syms_ = symbol.split("+")
        n_legs = len(_syms_)
        for _sym_ in _syms_:
            expected_investment = weight * total_tradable_money / n_legs
            if _sym_ in current_positions:
                expected_investment -= float(
                    get_position(trading_client, _sym_)["market_value"]
                )
                if expected_investment <= 0:
                    closing_positions[_sym_] = -expected_investment
                else:
                    opening_positions[_sym_] = expected_investment
            else:
                if weight > 0:
                    opening_positions[_sym_] = expected_investment
    opening_positions = dict(
        sorted(opening_positions.items(), key=lambda item: item[1], reverse=True)
    )
    return closing_positions, opening_positions


def submit_order(
    trading_client: TradingClient,
    order_request: MarketOrderRequest = None,
    symbol: str = None,
    qty: float = None,
    notional: float = None,
    side: str = None,
    type: str = "marker",
    time_in_force: str = "day",
    order_class=None,
    extended_hours=None,
    client_order_id=None,
    take_profit=None,
    stop_loss=None,
    wait_for_fill=False,
    timeout=60,
):
    """
    Submit an order to the trading_client.

    Args:
    trading_client: trading_client
    order_request: MarketOrderRequest
    symbol: str
    qty: float
    notional: float
    side: str
    type: str
    time_in_force: str
    order_class: str
    extended_hours: bool
    client_order_id: str
    take_profit: float
    stop_loss: float
    wait_for_fill: bool
    timeout: int
    """
    assert order_request is not None or (
        symbol is not None and qty is not None and side is not None
    ), "Either order_request or symbol, qty, side must be provided."
    if isinstance(order_request, list):
        return [
            submit_order(
                trading_client, order, wait_for_fill=wait_for_fill, timeout=timeout
            )
            for order in order_request
        ]
    if order_request is None:
        order_request = MarketOrderRequest(
            symbol=symbol,
            qty=qty,
            notional=notional,
            side=side,
            type=type,
            time_in_force=time_in_force,
            order_class=order_class,
            extended_hours=extended_hours,
            client_order_id=client_order_id,
            take_profit=take_profit,
            stop_loss=stop_loss,
        )
    try:
        order_details = trading_client.post(path="/orders", data=order_request.__dict__)
        if wait_for_fill:
            t0 = time()
            while order_details["status"] != "filled":
                order_details = trading_client.get(
                    path=f"/orders/{order_details['id']}"
                )
                sleep(1)
                if timeout is not None:
                    if time() - t0 > timeout:
                        logger.warning("Timeout reached.")
                        trading_client.delete(path=f"/orders/{order_details['id']}")
                        return None
    except Exception as e:
        logger.warning(e)
        return None
    return order_details


def get_current_w(TradingClient, asset_classes=["us_equity", "crypto", "us_option"]):
    positions = get_all_positions(TradingClient)
    positions = [pos for pos in positions if pos["asset_class"] in asset_classes]
    acc = TradingClient.get_account()
    total_tradable_money = float(acc.equity)
    curr_w = {pos["symbol"]: float(pos["market_value"]) for pos in positions}
    w = np.array(list(curr_w.values()))
    equity = w.sum()
    w /= total_tradable_money
    curr_w = dict(zip(curr_w.keys(), w))
    if w.sum() < 1:
        curr_w["_CASH_"] = 1 - w.sum()
    return curr_w, equity


def get_lastest_quote(symbols, paper=True):
    domain = "https://data.alpaca.markets/v1beta1/options/quotes/latest?symbols="
    if paper:
        key = os.getenv("APCA_API_KEY_ID")
        secret = os.getenv("APCA_API_SECRET_KEY")
    else:
        key = os.getenv("APCA_LIVE_API_KEY_ID")
        secret = os.getenv("APCA_LIVE_API_SECRET_KEY")

    headers = {
        "accept": "application/json",
        "APCA-API-KEY-ID": key,
        "APCA-API-SECRET-KEY": secret,
    }
    if isinstance(symbols, list):
        symbols = ",".join(symbols)
    response = requests.get(f"{domain}{symbols}&feed=indicative", headers=headers)
    return response.json()["quotes"]


def get_portfolio_history(paper=True, ndays=60, paper_profile="paper"):
    if paper:
        domain = "https://paper-api.alpaca.markets"
        if paper_profile != "paper":
            name_sep = f"_{paper_profile.upper()}_"
        else:
            name_sep = "_"
        key = os.getenv(f"APCA{name_sep}API_KEY_ID")
        secret = os.getenv(f"APCA{name_sep}API_SECRET_KEY")
    else:
        domain = "https://api.alpaca.markets"
        key = os.getenv("APCA_LIVE_API_KEY_ID")
        secret = os.getenv("APCA_LIVE_API_SECRET_KEY")

    headers = {
        "accept": "application/json",
        "APCA-API-KEY-ID": key,
        "APCA-API-SECRET-KEY": secret,
    }

    response = requests.get(
        os.path.join(domain, f"v2/account/portfolio/history?period={ndays}D"),
        headers=headers,
    )
    ans_df = pd.DataFrame(response.json())
    ans_df["date"] = (
        pd.to_datetime(ans_df["timestamp"], unit="s")
        .dt.tz_localize("UTC")
        .dt.tz_convert("America/Chicago")
    )

    try:
        ### Get cash deposits and withdraws
        response = requests.get(
            os.path.join(domain, "v2/account/activities/CSD"), headers=headers
        )
        cash_df = pd.DataFrame(response.json())
        if not cash_df.empty:
            cash_df["date_only"] = pd.to_datetime(cash_df["date"])
            ans_df["date_only"] = pd.to_datetime(ans_df["date"].map(lambda a: a.date()))
            ans_df = ans_df.merge(
                cash_df[["date_only", "net_amount"]], on="date_only", how="outer"
            )
            ans_df.net_amount = ans_df.net_amount.fillna(0).astype(float)
            ans_df["cumulative_profit_loss"] = 100 * (
                ans_df["equity"] / ans_df["net_amount"].cumsum() - 1
            )
        ans_df["profit_loss"] = ans_df["equity"].diff().fillna(0)
        ans_df["profit_loss_pct"] = ans_df["profit_loss"] / ans_df["equity"]
    except Exception as err:
        logger.warning(err)

    return ans_df.sort_values("date").drop(columns=["timestamp"])


def check_sym(sym, TradingClient: TradingClient, change_if_crypto: bool = True):
    try:
        a = TradingClient.get_asset(sym)
        if sym == a.symbol:
            return a
    except Exception as e:
        logger.warning(e)
        if change_if_crypto:
            new_sym = f"{sym}/USD"
            try:
                a = TradingClient.get_asset(new_sym)
            except Exception as e:
                logger.warning(e)
                return None
            if new_sym == a.symbol:
                logger.info(f"Changing {sym} to {new_sym}")
                return a
        else:
            return None
