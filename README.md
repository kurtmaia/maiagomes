# Uncertainty focused asset allocation


## Getting started

1. Clone the repo
```bash
git clone git@gitlab.com:kurtmaia/maiagomes.git
```
or
```bash
git clone https://gitlab.com/kurtmaia/maiagomes.git
```

2. Install poetry
```bash
pip install poetry 
```
3. Install package
```
poetry install
```
