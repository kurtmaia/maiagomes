library(shiny)
library(plyr)
library(dplyr)
library(stringr)
library(highcharter)

library(shinycssloaders)

library(reticulate)
setwd("../../")
source_python(file = "mmar/mmar.py")
pd <- import("pandas", convert = FALSE)

"%+%" <- function(arg1,arg2) paste0(arg1,arg2)

get_mode <- function(arr) names(which.max(table(arr)))

pathName = "mmar/app/dumps/"
if (!file.exists(pathName)){
    dir.create(pathName)
  }

pathName = pathName%+%Sys.Date()
if (!file.exists(pathName)){
    dir.create(pathName)
  }
preloadedSymbols <- list.files(pathName) %>% str_split("_") %>% sapply(function(x) x[[1]])

