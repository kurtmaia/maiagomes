#!/bin/bash


# Check if the required arguments are provided
if [ "$#" -ne 6 ]; then
    echo "Usage: $0 target_symbols_file cache_folder mmar_results_file paths_folder assessment_folder n_paths"
    exit 1
fi

# Assign command-line arguments to variables
target_symbols_file=$1
cache_folder=$2
mmar_results_file=$3
paths_folder=$4
assessment_folder=$5
n_paths=$6

########################
##### DATA PULLING #####
########################
maiagomes-data-puller -v --target-symbols "$target_symbols_file" --cache-folder "$cache_folder"

####################
##### MODELING #####
####################
maiagomes-estimate-mmar -v --mmar-results-file "$mmar_results_file" --target-symbols "$target_symbols_file" --cache-folder "$cache_folder" --persistent --parallel

### Option assessment
# maiagomes-option-assessment -v --mmar-results-file "$mmar_results_file" --paths-folder "$paths_folder" --assessment-folder "$assessment_folder" --persistent --rank-max 5 --parallel --n-paths "$n_paths" --weeks 1

####################
##### INVESTING ####
####################
# Add your investing logic here
