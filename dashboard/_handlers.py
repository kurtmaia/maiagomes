import json
import os
from datetime import datetime

import numpy as np
import pandas as pd
import streamlit as st
import umap
import yfinance as yf
from _utils import gen_design_matrix, parse_option_symbol
from alpaca.data.historical import CryptoHistoricalDataClient, StockHistoricalDataClient
from alpaca.trading.client import TradingClient
from scipy.stats import norm

from maiagomes.data.alpaca.pull import Pull_bars_from_alpaca
from maiagomes.models.allocate import (
    Allocate_arma,
    Allocate_options,
    get_arma_prediction,
    get_kelly_c,
    multivariate_kelly_allocation,
)
from maiagomes.models.mmar.montecarlo import MMAR_simulator_handler
from maiagomes.models.mmar.utils import moving_average
from maiagomes.models.options.pricing import Option_pricing_simulator


@st.cache_data(show_spinner="Pulling daily data...")
def get_daily_bars_from_alpaca(symbols):
    stock_client = StockHistoricalDataClient(
        os.getenv("APCA_API_KEY_ID"), os.getenv("APCA_API_SECRET_KEY")
    )
    crypto_client = CryptoHistoricalDataClient()
    latest_values = Pull_bars_from_alpaca(
        stock_client, crypto_client, start=datetime(2023, 1, 1)
    ).pull(symbols, crypto=["BTC/USD"], known_crypto_symbols=[])
    _df_ = latest_values.set_index("timestamp")
    lnmat = np.log(_df_.iloc[1:,]).diff(axis=0)[1:]
    lnmat_discretized = -1 * (lnmat < 0) + (lnmat > 0)
    weighted_graph = lnmat_discretized.T.dot(lnmat_discretized)
    weighted_graph_sum = (1.0 * (lnmat_discretized != 0)).T.dot(
        1.0 * (lnmat_discretized != 0)
    )
    bernoulli_graph = weighted_graph / weighted_graph_sum

    return _df_, bernoulli_graph


@st.cache_data(show_spinner="Embedding data in a low dimension vector space...")
def get_umap_embedding(bernoulli_graph):
    umap_model = umap.UMAP(n_components=3, n_neighbors=3)
    umap_result = umap_model.fit_transform(abs(bernoulli_graph.fillna(0)))
    umap_result_df = pd.DataFrame(
        umap_result[:, :2], columns=["proj1", "proj2"], index=bernoulli_graph.columns
    )
    return umap_result_df


def get_portfolio_weights(
    time_series_df_path="../investing_poc/data/mining/merged/all_quotes.csv",
    p=2,
    d=1,
    q=1,
    **kwargs,
):
    allocator = Allocate_arma(
        time_series_df_path=time_series_df_path,
        remap={"FB": "META"},
        blacklisted_symbols=["BNB", "XRP", "ADA", "PBR-A"],
    )
    stock_client = StockHistoricalDataClient(
        os.getenv("APCA_API_KEY_ID"), os.getenv("APCA_API_SECRET_KEY")
    )
    crypto_client = CryptoHistoricalDataClient()
    latest_values = Pull_bars_from_alpaca(
        stock_client, crypto_client, start=datetime(2023, 1, 1)
    ).pull(allocator.symbols)
    portfolio_weights = allocator.allocate(
        daily_df=latest_values.set_index("timestamp"), p=p, d=d, q=q, **kwargs
    )
    return portfolio_weights


@st.cache_data(show_spinner="Preping option pack...")
def get_option_pack(
    ticker_symbol,
    mmar_results=json.load(
        open(
            "../investing_poc/results/mining_with_mmar/latest_results.json",
            "rb",
        )
    ),
    # simulator_cache = {},
    expiration_date=None,
    n_paths=1000,
    try_pulling_cached_simulated_samples=True,
):
    days_until_expiration = (
        datetime.strptime(expiration_date, "%Y-%m-%d").date() - datetime.now().date()
    ).days
    days_until_expiration = int(
        days_until_expiration - 2 * np.floor(days_until_expiration / 7)
    )
    minutes_until_expiration = (days_until_expiration - 1) * 390
    if try_pulling_cached_simulated_samples:
        try:
            path_mats = np.load(f"results/simulated_paths/{ticker_symbol}.npy")
        except Exception as e:
            print(e)
            path_mats = None
    else:
        path_mats = None
    simulator = MMAR_simulator_handler(
        mmar_results[ticker_symbol], T=minutes_until_expiration
    )
    if path_mats is not None:
        simulator.mat = path_mats
    else:
        simulator.prep(n_paths=n_paths, parallel=True)
    ops = Option_pricing_simulator(simulator=simulator)
    ops.define(ticker_symbol=ticker_symbol, expiration_date=expiration_date)
    return ops


def get_exposure_survival_report(option_simulator, min_thr=420):
    _option_chain_ = option_simulator.option_chain_df.copy()
    with st.expander("Details"):
        st.dataframe(_option_chain_)
    selected_indices = st.multiselect("Select contracts:", _option_chain_.index)
    cols = st.columns(2)
    with cols[0]:
        precision = st.slider("Decimal place precision", 0.0, 1.0, 0.25, 0.001)
    with cols[1]:
        max_return = st.slider("Max return estimation", 1, 1000, 100, 1)
    plot_curves = st.button("Plot curves")
    if len(selected_indices) == 0 or not plot_curves:
        return None, None

    # Subset the dataframe with the selected indices
    # selected_rows = _option_chain_.loc[selected_indices]
    options, cost = option_simulator.prep_options_to_simulate_path(selected_indices)

    mybar = st.progress(0, "Running Black Scholes Merton pricing model...")
    mat = option_simulator.mmar_black_scholes_merton_paths(options=options)
    window_size = 30
    mat = moving_average(mat, window_size=window_size, axis=0)
    probs = {}
    returns = np.arange(1 + precision, max_return, precision)
    for i, x in enumerate(returns):
        if i % 100 == 0:
            mybar.progress(
                (i + 1) / returns.shape[0], f"Estimating {x}x return probability..."
            )
        survival_prob = option_simulator.simulator.get_path_probability(
            mat=mat, log_return=np.log(x), min_thr=min_thr, call_mode=True, Teff=1.0
        )
        probs[x] = survival_prob
        if survival_prob < 1e-2:
            break
    mybar.progress(100, "Done")
    return probs, cost


def get_option_upper_bound_report(option_simulator, min_thr=420):
    option_chain = option_simulator.option_chain_df
    y0 = option_chain["S"].iloc[0]
    upper_bound = {"call": {}, "put": {}}
    for row, col in option_chain.iterrows():
        strike_price = col.strike
        Xt = np.abs((strike_price / y0) * (1 - col.inTheMoney) - 1)
        p = option_simulator.simulator.get_path_probability(
            log_return=np.log(strike_price / y0),
            call_mode=col.direction == "call",
            min_thr=min_thr,
            Teff=1.0,
        )
        p_mono = option_simulator.simulator.get_path_probability(
            log_return=np.log(strike_price / y0),
            call_mode=col.direction == "call",
            expiration_only=True,
            Teff=1.0,
        )

        upper_bound[col.direction][strike_price] = {
            "upper_bound": Xt * y0 * p,
            "expiration_upper_bound": Xt * y0 * p_mono,
            "probability": p,
            "expiration_probability": p_mono,
        }

    upper_bound_df = (
        pd.concat({k: pd.DataFrame(v).T for k, v in upper_bound.items()})
        .reset_index()
        .rename(columns={"level_0": "direction", "level_1": "strike"})
    )
    upper_bound_df = upper_bound_df.merge(
        option_chain, on=["strike", "direction"], how="outer"
    )
    upper_bound_df["b"] = (
        np.abs(upper_bound_df["strike"] - y0) * (1 - upper_bound_df["inTheMoney"])
        - upper_bound_df["lastPrice"]
    ) / upper_bound_df["lastPrice"]
    upper_bound_df["kelly_c"] = upper_bound_df.apply(
        lambda a: get_kelly_c(p=a["probability"], b=a["b"]), axis=1
    )
    upper_bound_df["expiration_kelly_c"] = upper_bound_df.apply(
        lambda a: get_kelly_c(p=a["expiration_probability"], b=a["b"]), axis=1
    )
    upper_bound_df["ineff"] = (
        upper_bound_df["upper_bound"] - upper_bound_df["lastPrice"]
    ) / upper_bound_df["upper_bound"]
    upper_bound_df["expiration_ineff"] = (
        upper_bound_df["expiration_upper_bound"] - upper_bound_df["lastPrice"]
    ) / upper_bound_df["upper_bound"]
    upper_bound_df["current_stock_price"] = y0
    return option_simulator, upper_bound_df.sort_values(
        by=["inTheMoney", "strike_distance"]
    )


def get_arima_probability_at_expiration(row, daily_df):
    p = 0
    for cs in row.contractSymbol.split("+"):
        details = parse_option_symbol(cs)
        days_until_expiration = (
            datetime.strptime(details["expiration_date"], "%Y-%m-%d").date()
            - datetime.now().date()
        ).days
        preds = get_arma_prediction(
            daily_df[row.Symbol], p=2, d=1, q=1, steps=days_until_expiration
        )
        diff = ((-1) ** (details["option_type"] == "P")) * (
            preds.predicted_mean - details["strike_price"]
        )
        p += norm.cdf(diff / preds.se_mean)[-1]
    return p


def get_option_recommendation(
    assessment_df,
    bernoulli_graph,
    daily_df,
    univariate_allocation_variable="kelly_c",
    node_idx=None,
):
    strategy_df = assessment_df.copy()
    if node_idx is None:
        node_idx = strategy_df.sort_values(
            by=[univariate_allocation_variable, "expected_return"]
        ).index.tolist()[-1]
    allocator = Allocate_options(strategy_df.copy(), daily_df)
    allocator.assessment_df = strategy_df.copy()
    _p_ = (
        pd.DataFrame({"alloc": allocator.allocate(node_idx=node_idx)})
        .reset_index()
        .rename(columns={"index": "contractSymbol"})
    )
    strategy_df = strategy_df.merge(_p_, on="contractSymbol")
    strategy_df = strategy_df.query("alloc > 0")
    strategy_df["arima_prob_at_expiration"] = strategy_df.apply(
        lambda a: get_arima_probability_at_expiration(a, daily_df=daily_df), axis=1
    )
    return strategy_df


def get_daily_pl(trading_client, title="Daily P/L"):
    if isinstance(trading_client, dict):
        for k, v in trading_client.items():
            if k != "_LIVE_":
                title = f"Paper-{k}"
            else:
                title = "_LIVE_"
            get_daily_pl(v, title=title)
    else:
        acc = trading_client.get_account()
        eq = float(acc.equity)
        try:
            st.metric(
                title,
                f"${eq:,.2f}",
                delta=f"{(eq / float(acc.last_equity) - 1):.4%}",
            )
        except Exception as e:
            st.error(f"Problems with P/L metric: {e}")
