import json
from datetime import date, datetime, timedelta

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
import pytz
import streamlit as st
from scipy.integrate import simpson as simps
from scipy.stats import norm
from sklearn.neighbors import NearestNeighbors

from maiagomes.models.allocate import Allocate_arma, Allocate_mmar, get_kelly_c


def create_pnl_plot(df, length="all"):
    # Calculate cumulative profit/loss

    tz_chicago = pytz.timezone("America/Chicago")
    if length == "month":
        last_month_date = datetime.now() - timedelta(days=30)
        df = df[df["date_only"] >= tz_chicago.localize(last_month_date)]
    elif length == "week":
        last_week_date = datetime.now() - timedelta(days=7)
        df = df[df["date_only"] >= tz_chicago.localize(last_week_date)]
    elif length == "day":
        today_date = datetime.now() - timedelta(days=1)
        df = df[df["date_only"] >= tz_chicago.localize(today_date)]

    df["cumulative_profit_loss"] = 100 * (
        np.exp(np.log(df["profit_loss_pct"] + 1).cumsum()) - 1
    )

    # Create a P/L chart using Plotly
    fig = go.Figure()

    # Plot the cumulative profit/loss
    fig.add_trace(
        go.Scatter(
            x=df["date"],
            y=df["cumulative_profit_loss"],
            mode="lines+markers",
            name="Cumulative P/L (%)",
        )
    )

    # Plot the equity curve on the right y-axis
    fig.add_trace(
        go.Scatter(
            x=df["date"],
            y=df["equity"],
            mode="lines+markers",
            name="Equity Curve",
            yaxis="y2",
        )
    )

    # Customize layout
    fig.update_layout(
        title="Profit/Loss Chart",
        xaxis_title="Date",
        yaxis_title="Cumulative Profit/Loss (%)",
        showlegend=True,
        template="plotly_dark",
        yaxis2=dict(
            title="Equity Curve",
            titlefont=dict(color="#ff7f0e"),
            tickfont=dict(color="#ff7f0e"),
            # anchor="free",
            overlaying="y",
            side="right",
            # position=0.7,
        ),
    )

    # Show the plot
    return fig


def create_option_chart(
    df, reference_line=None, kelly_c_prefix="", title="Option Chart"
):

    fig = go.Figure()

    fig.add_trace(
        go.Scatter(
            x=df["strike"],
            y=df[f"{kelly_c_prefix}kelly_c"],
            mode="lines",
            name="Kelly c",
        )
    )

    fig.add_trace(
        go.Scatter(
            x=df["strike"],
            y=df[f"{kelly_c_prefix}ineff"],
            mode="lines",
            name="Inefficiency",
        )
    )

    fig.add_trace(
        go.Scatter(
            x=df["strike"],
            y=df[f"{kelly_c_prefix}upper_bound"],
            mode="lines",
            name="Price upper bound",
            yaxis="y2",
        )
    )

    fig.add_trace(
        go.Scatter(
            x=df["strike"],
            y=df["lastPrice"],
            mode="lines",
            name="Option price",
            yaxis="y2",
        )
    )
    if reference_line is not None:
        fig.add_shape(
            go.layout.Shape(
                type="line",
                x0=reference_line,
                x1=reference_line,
                line=dict(color="black", width=2),
            )
        )

    # Customize layout
    fig.update_layout(
        title=title,
        xaxis_title="Strike",
        yaxis_title="Kelly C",
        # yaxis_range=[-0.1,1.1],
        showlegend=True,
        template="plotly_dark",
        yaxis2=dict(
            title="Option Price",
            titlefont=dict(color="#ff7f0e"),
            tickfont=dict(color="#ff7f0e"),
            # anchor="free",
            overlaying="y",
            side="right",
        ),
    )

    return fig


def create_allocation_chart(data, data2=None, title="Allocation"):
    if isinstance(data, dict):
        data = (
            pd.DataFrame({"size": data})
            .reset_index()
            .rename(columns={"index": "symbol"})
        )
    if data2 is not None:
        data = data.merge(data2, on="symbol", suffixes=["_new", "_old"])
        data["size"] = data["size_new"] - data["size_old"]
    # Create an interactive pie chart with Plotly
    _type_ = data["symbol"].map(lambda a: "cash" if a == "_CASH_" else "stock").values

    fig = px.bar(data, y="size", x="symbol", color=_type_, title=title)

    # Display the chart in Streamlit
    st.plotly_chart(
        fig,
        use_container_width=True,
    )


def contract_assessment_plot(
    df,
    y="kelly_c",
    x="expected_return",
    size="cost",
    color="Symbol",
    min_prob=0.4,
    min_expected_return=1.0,
    title=None,
    last_update_time="",
):
    if title is None:
        title = f"Assessment Plot: Last updated on {last_update_time}"
    fig = px.scatter(
        data_frame=df.query(
            f"probability > {min_prob} and expected_return >= {min_expected_return}"
        ),
        y=y,
        x=x,
        size=size,
        color=color,
        symbol="type",
        title=title,
        hover_data=["probability", "contractSymbol"],
    )

    return fig


def plot_umap(df, focused_symbols=None):
    if focused_symbols is not None:
        _df_ = df.loc[focused_symbols,].reset_index()
    else:
        _df_ = df.reset_index()

    # Assume you have columns 'proj1' and 'proj2' as your coordinates
    coordinates = _df_[["proj1", "proj2"]].values

    # Number of neighbors to consider
    n_neighbors = 3
    # Fit a Nearest Neighbors model
    neighbors_model = NearestNeighbors(n_neighbors=n_neighbors, algorithm="auto")
    neighbors_model.fit(coordinates)

    # Find nearest neighbors for each point
    distances, indices = neighbors_model.kneighbors(coordinates)

    # Add distances to the DataFrame
    _df_["nearest_neighbors"] = [
        list(zip(indices[i], distances[i])) for i in range(len(_df_))
    ]

    # Create the scatter plot
    fig = px.scatter(
        _df_,
        y="proj1",
        x="proj2",
        color="symbol",
        text="symbol",
        hover_data={"symbol": True, "nearest_neighbors": False},
    )

    fig.update_traces(textposition="bottom center")

    if st.toggle("Add nearnest neighbor lines"):
        for i, row in _df_.iterrows():
            for neighbor_idx, distance in row["nearest_neighbors"]:

                neighbor = _df_.iloc[neighbor_idx]
                fig.add_scatter(
                    x=[row["proj2"], neighbor["proj2"]],
                    y=[row["proj1"], neighbor["proj1"]],
                    line=dict(color="grey", width=np.log(1 + 1 / distance)),
                    showlegend=False,
                )

    return fig


def plot_similarity_heatmap(df):
    _df_ = df.dropna()
    lnmat = np.log(_df_.iloc[1:,]).diff(axis=0)[1:]
    lnmat_discretized = -1 * (lnmat < 0) + (lnmat > 0)
    weighted_graph = lnmat_discretized.T.dot(lnmat_discretized)
    weighted_graph_sum = (1.0 * (lnmat_discretized != 0)).T.dot(
        1.0 * (lnmat_discretized != 0)
    )
    bernoulli_graph = weighted_graph / weighted_graph_sum
    return bernoulli_graph


def hurst_plot(latest_results):
    df = (
        pd.DataFrame(latest_results).T.reset_index().rename(columns={"index": "ticker"})
    )
    fig = px.scatter(
        data_frame=df,
        x="alpha0",
        y="H",
        # size='N',
        color="ticker",
        labels={"x": "local H", "y": "Hurst Exponent"},
        text="ticker",
        title="Hurst Scatter Plot",
        # hover_data={"size": 'N', "color": 'H', 'x' : 'ticker'}
    )
    fig.update_traces(textposition="bottom center")

    return fig


def exposure_plot(probs, lastPrice=None):
    ans = (
        pd.DataFrame({"Probability": probs})
        .reset_index()
        .rename(columns={"index": "return"})
    )
    ans["kelly_c"] = ans.apply(
        lambda a: get_kelly_c(p=a["Probability"], b=a["return"] - 1), axis=1
    )

    fig = go.Figure()

    fig.add_trace(
        go.Scatter(
            x=ans["return"],
            y=ans["Probability"],
            mode="lines",
            name="Probability",
        )
    )

    fig.add_trace(
        go.Scatter(
            x=ans["return"],
            y=ans["kelly_c"],
            mode="lines",
            name="Kelly C",
        )
    )

    if lastPrice is not None:
        ans["Exposure"] = ans.apply(
            lambda a: (1 - a["Probability"]) * lastPrice, axis=1
        )
        returns_mat = ans["return"].values
        probs_mat = ans["Probability"].values
        expected_return = simps(probs_mat, x=returns_mat)
        second_moment = 2 * simps(returns_mat * probs_mat, x=returns_mat)
        variance = second_moment - expected_return**2
        expected_lower_bound = np.exp(
            norm.ppf(0.05, np.log(expected_return), np.log(np.sqrt(variance)))
        )
        expected_upper_bound = np.exp(
            norm.ppf(0.95, np.log(expected_return), np.log(np.sqrt(variance)))
        )
        P_not_lose = norm.sf(
            np.log(2), np.log(expected_return), np.log(np.sqrt(variance))
        )
        fig.add_trace(
            go.Scatter(
                x=ans["return"],
                y=ans["Exposure"],
                mode="lines",
                name="Exposure",
                yaxis="y2",
            )
        )

        fig.update_layout(
            title=f"Expected return: {expected_return:.3f} [{expected_lower_bound:.2f},{expected_upper_bound:.2f}] | Investment cost per contract: ${lastPrice*100:,.2f}.",
            xaxis=dict(
                title="Return",
                showspikes=True,
                spikemode="across",
                spikesnap="cursor",
                showline=True,
                showgrid=True,
            ),
            yaxis_title="Probability+Kelly C",
            # yaxis_range=[-0.1,1.1],
            showlegend=True,
            template="plotly_dark",
            hovermode="x",
            yaxis2=dict(
                title="Exposure * 100 ($)",
                titlefont=dict(color="#ff7f0e"),
                tickfont=dict(color="#ff7f0e"),
                # anchor="free",
                overlaying="y",
                side="right",
            ),
        )

    return fig
