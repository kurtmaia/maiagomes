import json
import re
import subprocess
from datetime import date, datetime, timedelta

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
import streamlit as st

from maiagomes.models.allocate import Allocate_arma, Allocate_mmar, get_kelly_c


def is_launchctl_running(process):
    try:
        output = (
            subprocess.check_output(["launchctl", "list"], stderr=subprocess.STDOUT)
            .decode("utf-8")
            .splitlines()
        )
        task = [task for task in output if task.find(process) > -1]
        assert len(task) == 1
        vals = task[0].split("\t")
        if vals[0] != "-":
            return vals[0]
        else:
            return int(vals[1])
    except Exception as e:
        return 1


def gen_design_matrix(arr, cats=None, **kwargs):
    nrow = len(arr)
    if cats is None:
        cats = np.unique(arr)
    ncol = len(cats)
    cats_dict = dict(zip(cats, range(ncol)))
    M = np.zeros([nrow, ncol], **kwargs)
    for i in range(nrow):
        M[i, cats_dict[arr[i]]] = 1.0
    return M


def prepare_input():
    st.header("Input uploader")

    # Sidebar for file type selection

    # File uploader
    uploaded_file = st.file_uploader(f"Upload file", type=[".csv", ".json"])

    if uploaded_file is not None:
        st.success("File uploaded successfully!")

        # Display uploaded data
        if uploaded_file.name.endswith(".csv"):
            return Allocate_arma(
                time_series_df_path=uploaded_file,
                remap={"FB": "META"},
                blacklisted_symbols=["BNB", "XRP", "ADA", "PBR-A"],
            )
        elif uploaded_file.name.endswith(".json"):
            mmar_results = json.load(uploaded_file)
            return Allocate_mmar(
                mmar_results=mmar_results,
                remap={"FB": "META"},
                blacklisted_symbols=["BNB", "XRP", "ADA", "PBR-A"],
            )
    else:
        return None


def closest_friday(given_date=datetime.now().date()):
    # Calculate the difference in days between the given date and Friday (weekday 4)
    days_until_friday = (4 - given_date.weekday() + 7) % 7

    # Calculate the date of the closest Friday
    closest_friday_date = given_date + timedelta(days=days_until_friday)

    return closest_friday_date


def parse_option_symbol(option_symbol):
    # Define the regular expression pattern to extract components
    pattern = r"([A-Z]+)(\d{6})([CP])(\d{8})"

    # Match the pattern against the option symbol
    match = re.match(pattern, option_symbol)

    if match:
        # Extract the components from the matched groups
        underlying_symbol = match.group(1)
        expiration_date = match.group(2)
        option_type = match.group(3)
        strike_price = match.group(4)

        # Format the expiration date in YYYY-MM-DD format
        expiration_date_formatted = (
            f"20{expiration_date[:2]}-{expiration_date[2:4]}-{expiration_date[4:]}"
        )

        return {
            "underlying_symbol": underlying_symbol,
            "expiration_date": expiration_date_formatted,
            "option_type": option_type,
            "strike_price": float(strike_price) / 1000,
        }
    else:
        return None
