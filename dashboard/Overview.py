import json
import os

import numpy as np
import pandas as pd
import streamlit as st
from _handlers import get_daily_pl, get_portfolio_weights
from _plots import create_allocation_chart, create_pnl_plot
from _utils import is_launchctl_running, prepare_input
from alpaca.trading.client import TradingClient
from dotenv import load_dotenv

from maiagomes.investing.portfolio import RebalancePortfolio
from maiagomes.investing.portfolio.utils import get_current_w, get_portfolio_history

load_dotenv()


def get_trading_clients():
    trading_clients = {}
    for client_type_sep in ["_", "_MID_", "_HIGH_", "_LIVE_"]:
        trading_clients[client_type_sep] = TradingClient(
            os.getenv(f"APCA{client_type_sep}API_KEY_ID"),
            os.getenv(f"APCA{client_type_sep}API_SECRET_KEY"),
            paper=client_type_sep != "_LIVE_",
        )
    return trading_clients


if "trading_clients" not in st.session_state.keys():
    st.session_state["trading_clients"] = get_trading_clients()

with st.sidebar:
    client_type = st.radio("Client type:", ["paper", "live"])
    if client_type == "paper":
        paper = True
        client_profile = st.radio("Paper profile:", ["paper", "mid", "high"])
        if client_profile == "paper":
            client_type_sep = "_"
        else:
            client_type_sep = f"_{client_profile.upper()}_"
    else:
        client_type_sep = "_LIVE_"
        paper = False
        client_profile = "live"

    exit = is_launchctl_running("guilhermegomes.mmar")
    if exit == 0:
        st.success("MMAR pipeline ran successfully ✅")
    elif exit == 1:
        st.error("MMAR pipeline had an error 🚨")
    else:
        st.info("MMAR pipeline is running 🏃🏼‍♂️‍➡️")
    exit = is_launchctl_running("guilhermegomes.investor")
    if exit == 0:
        st.success("Investor pipeline ran successfully ✅")
    elif exit == 1:
        st.error("Investor pipeline had an error 🚨")
    else:
        st.info("Investor pipeline is running 🏃🏼‍♂️‍➡️")

    st.sidebar.markdown("---")
    st.header("Performance")
    get_daily_pl(st.session_state["trading_clients"])


st.title("Alpaca dashboard")

with st.container():
    acc = st.session_state["trading_clients"][client_type_sep].get_account()
    paper = client_type_sep != "_LIVE_"
    eq = float(acc.equity)
    try:
        st.metric(
            "Daily P/L",
            f"${eq:,.2f}",
            delta=f"{(eq / float(acc.last_equity) - 1):.4%}",
        )
    except Exception as e:
        st.error(f"Problems with P/L metric: {e}")
    ndays = st.slider("Number of days", 30, 360, 60, 30)
    acc_performance = get_portfolio_history(
        paper=paper, ndays=ndays, paper_profile=client_profile
    )
    try:
        # performance_length = st.selectbox(
        #     "Performance length", ["all", "month", "week", "day"]
        # )
        st.plotly_chart(
            create_pnl_plot(acc_performance),
            use_container_width=True,
        )
    except Exception as e:
        st.error(f"Problems plotting performance: {e}")
    with st.expander("See details"):
        st.write(acc_performance)


with st.container():
    st.header("Re-balancer")
    rebalancer = RebalancePortfolio(
        st.session_state["trading_clients"][client_type_sep]
    )
    curr_w, eq = get_current_w(st.session_state["trading_clients"][client_type_sep])
    df = (
        pd.DataFrame({"size": curr_w}).reset_index().rename(columns={"index": "symbol"})
    )
    create_allocation_chart(df, title="Current allocation")
    st.subheader("Arima parameters")
    arima_cols = st.columns(3)
    with arima_cols[0]:
        p = st.number_input("p", 0, value=2)
    with arima_cols[1]:
        d = st.number_input("d", 0, value=1)
    with arima_cols[2]:
        q = st.number_input("q", 0, value=1)
    if st.button("Create new allocation"):
        st.session_state["portfolio_weights"] = get_portfolio_weights(p=p, d=d, q=q)
    if "portfolio_weights" in st.session_state.keys():
        create_allocation_chart(
            st.session_state["portfolio_weights"], title="New allocation"
        )
        if st.button("Allocate"):
            my_bar = st.progress(0, "Preparing orders...")
            rebalancer.prep_orders(
                portfolio_weights=st.session_state["portfolio_weights"]
            )
            my_bar.progress(50, "Submitting order...")
            rebalancer.run()
            my_bar.progress(100, "Re-allocation completed")
