import json

import numpy as np
import pandas as pd
import streamlit as st
from _handlers import (
    get_exposure_survival_report,
    get_option_pack,
    get_option_upper_bound_report,
)
from _plots import hurst_plot

st.title("MMAR explorer")

# with st.container():
#     latest_results = json.load(
#         open("../investing_poc/results/mining_with_mmar/latest_results.json", "rb")
#     )
#     st.plotly_chart(
#         hurst_plot(latest_results),
#         use_container_width=True,
#     )
#     st.dataframe(pd.DataFrame(latest_results).T)


with st.container():
    latest_results = json.load(open("results/mmar/latest_results.json", "rb"))
    st.plotly_chart(
        hurst_plot(latest_results),
        use_container_width=True,
    )
    st.dataframe(pd.DataFrame(latest_results).T, use_container_width=True)
