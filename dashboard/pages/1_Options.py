import json
import os
import subprocess
import time

import numpy as np
import pandas as pd
import streamlit as st
from _handlers import (
    get_daily_bars_from_alpaca,
    get_exposure_survival_report,
    get_option_pack,
    get_option_recommendation,
    get_option_upper_bound_report,
    get_umap_embedding,
)
from _plots import (
    contract_assessment_plot,
    create_allocation_chart,
    create_option_chart,
    exposure_plot,
    plot_similarity_heatmap,
    plot_umap,
)
from _utils import closest_friday

if "option_report_df" not in st.session_state.keys():
    st.session_state["option_report_df"] = None

st.title("Options report")

with st.container():
    assessment_df = (
        pd.read_csv("results/assessment/_full_.csv")
        .rename(columns={"Unnamed: 0": "Symbol"})
        .dropna(subset="contractSymbol")
    )
    with st.expander("Details"):
        st.dataframe(assessment_df)
    last_update_time = time.ctime(os.path.getmtime("results/assessment/_full_.csv"))
    assess_setting_cols = st.columns(2)
    with assess_setting_cols[0]:
        min_prob = st.slider("Min probability", 0.0, 1.0, 0.4, 0.01)
    with assess_setting_cols[1]:
        min_expected_return = st.number_input("Min expected return", value=1.0)
    if st.button("Get daily"):
        st.session_state["daily_df"], st.session_state["bernoulli_graph"] = (
            get_daily_bars_from_alpaca(list(set(assessment_df.Symbol)))
        )
    tabs = st.tabs(["Assessment", "Ticker correlation", "Recommendation"])
    with tabs[0]:
        _assessment_cols = st.columns(2)
        with _assessment_cols[0]:
            variables = np.array(list(assessment_df.columns))
            _x_ = st.selectbox(
                "x", variables, index=int((variables == "expected_return").argmax())
            )
        with _assessment_cols[1]:
            variables = np.array(list(set(assessment_df.columns) - set([_x_])))
            _y_ = st.selectbox(
                "y", variables, index=int((variables == "kelly_c").argmax())
            )
        st.plotly_chart(
            contract_assessment_plot(
                assessment_df,
                x=_x_,
                y=_y_,
                min_prob=min_prob,
                min_expected_return=min_expected_return,
                last_update_time=last_update_time,
            ),
            use_container_width=True,
        )
    if "daily_df" in st.session_state.keys():
        with tabs[1]:
            focused_symbols = list(
                set(
                    assessment_df.query(
                        f"probability > {min_prob} and expected_return >= {min_expected_return}"
                    ).Symbol
                )
            )
            with st.expander("Show Bernoulli graph"):
                st.dataframe(
                    plot_similarity_heatmap(
                        st.session_state["daily_df"][focused_symbols]
                    )
                )
            if st.button("Get embeddings"):

                st.session_state["stock_umap"] = get_umap_embedding(
                    st.session_state["bernoulli_graph"]
                )
            if "stock_umap" in st.session_state.keys():
                st.plotly_chart(
                    plot_umap(
                        st.session_state["stock_umap"], focused_symbols=focused_symbols
                    ),
                    use_container_width=True,
                )
            with tabs[2]:
                min_cost = st.slider("Min cost", 0.0, value=0.3, step=0.05)
                assessment_df_over1 = assessment_df.query(
                    f"expected_return > 1 and cost > {min_cost}"
                ).reset_index(drop=True)
                univariate_allocation_variable = st.radio(
                    "Univariate allocation variable",
                    ["kelly_c", "kelly_c_50", "kelly_c_100"],
                )
                if st.checkbox("Choose a starting contract"):
                    contr = st.selectbox(
                        "Choose a starting contract:",
                        assessment_df_over1.contractSymbol,
                    )
                    node_idx = assessment_df_over1.query(
                        f"contractSymbol == '{contr}' "
                    ).index.tolist()[0]
                else:
                    node_idx = None

                strategy_df = get_option_recommendation(
                    assessment_df_over1,
                    st.session_state["bernoulli_graph"],
                    st.session_state["daily_df"],
                    univariate_allocation_variable=univariate_allocation_variable,
                    node_idx=node_idx,
                )
                create_allocation_chart(
                    strategy_df.set_index("contractSymbol").alloc.to_dict(),
                    title=f"Total allocation: {strategy_df.alloc.sum():.3f}",
                )
                st.plotly_chart(
                    contract_assessment_plot(
                        strategy_df,
                        x="alloc",
                        y="arima_prob_at_expiration",
                        size="cost",
                        min_prob=0,
                        min_expected_return=1,
                        title="Expiration probability vs allocation",
                    ),
                    use_container_width=True,
                )
                with st.expander("Recommendation details"):
                    st.dataframe(strategy_df)

    latest_results = json.load(open("results/mmar/latest_results.json", "rb"))
    onlyStocks = list(
        set(list(latest_results.keys()))
        - set(["BTC", "ETH", "BNB", "SOL", "XRP", "ADA", "LUNA"])
    )
    cols = st.columns(2)
    with cols[0]:
        _cols_ = st.columns(2)
        with _cols_[0]:
            ticker = st.selectbox("Select a ticker:", options=onlyStocks)
        with _cols_[1]:
            expiration_date = st.date_input(
                "Expiration date", format="YYYY-MM-DD", value=closest_friday()
            )
        try_pulling_cached_simulated_samples = st.toggle(
            "Try pulling cached simulated samples", value=False
        )
    with cols[1]:
        n_paths = st.slider(
            "Number of paths to simulate",
            100,
            10000,
            5000,
            100,
            help="The larger the path, more paths are needed.",
        )
        min_thr = st.slider("Min time", 0, 1024, 390, 10)

    if st.button("Get report"):
        option_pack = get_option_pack(
            ticker,
            mmar_results=latest_results,
            expiration_date=str(expiration_date),
            n_paths=n_paths,
            try_pulling_cached_simulated_samples=try_pulling_cached_simulated_samples,
        )
        st.session_state["simulator"], st.session_state["option_report_df"] = (
            get_option_upper_bound_report(
                option_simulator=option_pack,
                min_thr=min_thr,
            )
        )

with st.container():
    if st.session_state["option_report_df"] is not None:
        exposure, lastPrice = get_exposure_survival_report(
            st.session_state["simulator"],
            min_thr=min_thr,
        )
        if exposure is not None:
            st.plotly_chart(
                exposure_plot(exposure, lastPrice=lastPrice),
                use_container_width=True,
            )
        with st.expander("Details"):
            expiration_only = st.toggle("Expiration probabilities")
            outthemoney = st.toggle("Out-of-The-Money only")
            current_stock_price = st.session_state["option_report_df"][
                "current_stock_price"
            ].iloc[0]
            if outthemoney:
                st.plotly_chart(
                    create_option_chart(
                        st.session_state["option_report_df"].query("not inTheMoney"),
                        reference_line=current_stock_price,
                        kelly_c_prefix="expiration_" if expiration_only else "",
                        title="Option chart",
                    ),
                    use_container_width=True,
                )
            else:
                tabs = st.tabs(["Calls", "Puts"])
                for i, tab in enumerate(tabs):
                    with tab:
                        direction = "call" if i == 0 else "put"
                        _type_ = "Calls" if i == 0 else "Puts"
                        st.plotly_chart(
                            create_option_chart(
                                st.session_state["option_report_df"].query(
                                    f"direction == '{direction}'"
                                ),
                                reference_line=current_stock_price,
                                kelly_c_prefix="expiration_" if expiration_only else "",
                                title=_type_,
                            ),
                            use_container_width=True,
                        )

            st.write(st.session_state["option_report_df"])
